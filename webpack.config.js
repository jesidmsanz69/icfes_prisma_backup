/* eslint-disable no-param-reassign */
/* eslint-disable operator-linebreak */
/* eslint-disable space-before-function-paren */
/* eslint-disable indent */
const path = require('path');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const autoprefixer = require('autoprefixer');
const webpack = require('webpack');
//Compirmir el js
// const TerserPlugin = require('terser-webpack-plugin');
//Para comprimir los assets
const CompressionPlugin = require('compression-webpack-plugin');
const dotenv = require('dotenv');
const LoadablePlugin = require('@loadable/webpack-plugin');
const ImageminWebpWebpackPlugin = require('imagemin-webp-webpack-plugin');
//Para manejar el hash
// const ManifestPlugin = require('webpack-manifest-plugin');

dotenv.config();
const ENV = process.env.NODE_ENV;
const isProd = ENV === 'production';
// call dotenv and it will return an Object with a parsed key
const envParsed = dotenv.config().parsed;

// reduce it to a nice object, the same as before
const envKeys = Object.keys(envParsed).reduce((prev, next) => {
  prev[`process.env.${next}`] = JSON.stringify(envParsed[next]);
  return prev;
}, {});
module.exports = {
  devtool: isProd ? 'hidden-source-map' : 'cheap-source-map',
  entry: isProd
    ? './src/frontend/index.js'
    : [
        './src/frontend/index.js',
        'webpack-hot-middleware/client?path=/__webpack_hmr&timeout=2000&reload=true',
      ],
  mode: ENV,
  output: {
    path: path.join(process.cwd(), './src/server/public') ,
    filename: isProd ? '[name]-bundle-[chunkhash:8].js' : 'assets/[name].js',
    publicPath: '/',
  },
  resolve: {
    extensions: ['.js', '.jsx'],
  },
  // optimization: !isProd
  //   ? {
  //       splitChunks: {
  //         // cacheGroups: {
  //         //   admin: {
  //         //     name: 'admin',
  //         //     test: /admin(.*)\.s?css$/,
  //         //     chunks: 'all',
  //         //     enforce: true,
  //         //   },
  //         // },
  //       },
  //     }
  //   : {
  //       minimize: true,
  //       minimizer: isProd ? [new TerserPlugin()] : [],
  //       splitChunks: {
  //         chunks: 'async',
  //         name: true,
  //         cacheGroups: {
  //           vendors: {
  //             name: 'vendors',
  //             chunks: 'all',
  //             reuseExistingChunk: true,
  //             priority: 1,
  //             filename: isProd ? 'assets/vendor-[hash].js' : 'assets/vendor.js',
  //             enforce: true,
  //             test(module, chunks) {
  //               const name = module.nameForCondition && module.nameForCondition();
  //               return chunks.some(
  //                 (chunk) => chunk.name !== 'vendors' && /[\\/]node_modules[\\/]/.test(name)
  //               );
  //             },
  //           },
  //           // admin: {
  //           //   name: 'admin',
  //           //   test: /admin(.*)\.s?css$/,
  //           //   chunks: 'all',
  //           //   enforce: true,
  //           // },
  //         },
  //       },
  //     },
  module: {
    rules: [
      // {
      //   test: /\.(js|jsx)$/,
      //   exclude: /node_modules/,
      //   enforce: 'pre',
      //   use: {
      //     loader: 'eslint-loader',
      //   },
      // },
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
        },
      },
      // {
      //   test: /\.(s*)css$/,
      //   use: [
      //     { loader: MiniCssExtractPlugin.loader },
      //     'css-loader',
      //     'postcss-loader',
      //     {
      //       loader: 'sass-loader',
      //       options: {
      //         prependData: `
      //         @import "src/frontend/assets/styles/Vars.scss";
      //         @import "~bootstrap/scss/bootstrap.scss";
      //         @import "src/frontend/assets/styles/Media.scss";
      //         @import "src/frontend/assets/styles/Base.scss";
      //         `,
      //       },
      //     },
      //   ],
      // },
      {
        test: /\.(s*)css$/,
        use: [
          {
            loader: MiniCssExtractPlugin.loader,
            options: {
              modules: {
                mode: (resourcePath) => {
                  if (/pure.css$/i.test(resourcePath)) {
                    return 'pure';
                  }

                  if (/global.css$/i.test(resourcePath)) {
                    return 'global';
                  }

                  return 'local';
                },
              },
            },
          },
          'css-loader',
          // 'css-loader?importLoader=1&modules',
          'sass-loader',
        ],
      },
      // {
      //   test: /\.svg$/,
      //   use: [
      //     {
      //       loader: 'svg-url-loader',
      //       options: {
      //         limit: 10000,
      //         name: 'assets/[hash].[ext]',
      //       },
      //     },
      //   ],
      // },
      {
        test: /\.(webp)$/i,
        use: [
          {
            loader: 'file-loader',
            options: { name: 'assets/[hash]/[name].[ext]' },
          },
          // {
          //   loader: 'webp-loader',
          //   options: { quality: 100 },
          // },
        ],
      },
      {
        test: /\.(png|gif|jpg|jpeg|svg)$/i,
        use: [
          {
            loader: 'file-loader',
            options: { name: 'assets/[hash]/[name].[ext]' },
          },
        ],
      },
    ],
  },
  devServer: {
    // //Para trabajar en dev con las rutas de react-router-dom
    // historyApiFallback: true,
    // //contentBase: path.join(__dirname, 'dist'),
    // contentBase: './src/frontend',
    // compress: false,
    // port: 9000,
    // contentBasePublicPath: '/public',
    // lazy: true,
    // filename: 'assets/app.js',
    // publicPath: '/',
    contentBase: `${__dirname}/public/`,
    publicPath: '/',
    port: 8080,
    stats: 'minimal',
    watchContentBase: true,
    historyApiFallback: true,
    open: false,
    hot: false,
  },
  plugins: [
    new webpack.DefinePlugin(envKeys),
    new LoadablePlugin(),
    new ImageminWebpWebpackPlugin({config: [{
      test: /\.(jpe?g|png)/,
      options: {
        quality:  85
      }
    }],}),
    isProd ? function() {} : new webpack.HotModuleReplacementPlugin(),
    new webpack.LoaderOptionsPlugin({
      options: {
        postcss: [autoprefixer()],
      },
    }),
    new MiniCssExtractPlugin({
      filename: isProd ? 'assets/[name]-[hash].css' : 'assets/[name].css',
    }),
    isProd
      ? new CompressionPlugin({
          test: /\.js$|\.css$/,
          filename: '[path].gz',
        })
      : function() {},
    // isProd ? new ManifestPlugin() : function() {},
  ],
  node: { fs: 'empty' },
};
