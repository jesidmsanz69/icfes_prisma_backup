const redirects = [
  {
    from: '/about-us',
    to: '/about',
  },
  {
    from: '/our-projects',
    to: '/projects',
  },
  {
    from: '/contact-us',
    to: '/contact',
  },
];

module.exports = redirects;
