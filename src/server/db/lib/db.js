const Sequelize = require('sequelize');

let sequelize = null;
let sequelizeApp = null;

module.exports = function setupDatabase(config, dbId = 1) {
  // console.log('setupDatabase config', config);
  // console.log('sequelize', sequelize);
  if (dbId === 2) {
    if (!sequelizeApp) {
      sequelizeApp = new Sequelize(config);
    }
    return sequelizeApp;
  }
  if (!sequelize) {
    sequelize = new Sequelize(config);
  }
  return sequelize;
};
