const configEnv = require('../../config').config;

const config = {
  development: {
    database: configEnv.dbName,
    username: configEnv.dbUser,
    password: configEnv.dbPassword,
    host: configEnv.dbHost,
    dialect: configEnv.defaultDialect,
  },
  test: {
    database: configEnv.dbName,
    username: configEnv.dbUser,
    password: configEnv.dbPassword,
    host: configEnv.dbHost,
    dialect: configEnv.defaultDialect,
  },
  production: {
    database: configEnv.dbName,
    username: configEnv.dbUser,
    password: configEnv.dbPassword,
    host: configEnv.dbHost,
    dialect: configEnv.defaultDialect,
  },
};
if (configEnv.defaultDialect === 'sqlite') {
  config.development.storage = `./src/server/db/${configEnv.dbName}.sqlite`;
  config.development.pool = {
    max: 10,
    min: 0,
    idle: 10000,
  };
  config.development.query = {
    raw: true,
  };
  config.test.storage = config.development.storage;
  config.test.pool = config.development.pool;
  config.test.query = config.development.query;

  config.production.storage = config.development.storage;
  config.production.pool = config.development.pool;
  config.production.query = config.development.query;
}
module.exports = config;
