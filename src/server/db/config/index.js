const configEnv = require('../../config').config;

// const debug = require('debug')('lavtime:db:setup');
/**
 * CREATE ROLE lavtime WITH LOGIN PASSWORD 'lav1402';
 * CREATE DATABASE lavtime_db;
 * GRANT ALL PRIVILEGES ON DATABASE lavtime_db TO lavtime;
 */

const config = {
  database: configEnv.dbName,
  username: configEnv.dbUser,
  password: configEnv.dbPassword,
  host: configEnv.dbHost,
  dialect: configEnv.defaultDialect,
  // logging: (s) => debug(s),
  logging: console.log,
};
if (configEnv.defaultDialect === 'sqlite') {
  config.storage = `./src/server/db/${configEnv.dbName}.sqlite`;
  config.pool = {
    max: 10,
    min: 0,
    idle: 10000,
  };
  config.query = {
    raw: true,
  };
}
// const config = `sqlite:relativePath/src/${configEnv.dbName}.db`;

const configAppIT49 = {
  database: configEnv.dbAppName,
  username: configEnv.dbAppUser,
  password: configEnv.dbAppPassword,
  host: configEnv.dbAppHost,
  dialect: 'mssql',
  port: configEnv.dbAppPort,
  // logging: (s) => debug(s),
  logging: console.log,
  options: {
    encrypt: true,
    enableArithAbort: true,
  },
};

module.exports = { config, configAppIT49 };
