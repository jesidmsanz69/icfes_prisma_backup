/* eslint-disable prefer-destructuring */
// const red = require('chalk').red;
// const green = require('chalk').green;
const inquirer = require('inquirer');

const createPromptModule = inquirer.createPromptModule;
// const debug = require('debug')('lavtime:db:setup');
// const configEnv = require('../config').config;
const { config } = require('./config');
const db = require('./models');

const prompt = createPromptModule();

function handleFatalError(err) {
  console.error(`[FATAL ERROR] ${err.message}`);
  console.error(err.stack);
  process.exit(1);
}

async function setup() {
  const answer = await prompt({
    type: 'confirm',
    name: 'setup',
    message: 'This will destroy your database, are you sure?',
  });

  if (!answer.setup) {
    return console.log('Nothing happened!');
  }
  // console.log('configEnv', configEnv);
  // const config = {
  //   database: configEnv.dbName,
  //   username: configEnv.dbUser,
  //   password: configEnv.dbPassword,
  //   host: configEnv.dbHost,
  //   dialect: configEnv.defaultDialect,
  //   logging: (s) => console.log(s),
  //   setup: true,
  // };
  config.setup = true;
  console.log('setup config', config);
  await db(config).catch(handleFatalError);

  console.log('Success');
  process.exit(0);
}

setup();
