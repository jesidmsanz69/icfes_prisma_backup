'use strict';

// const defaults = require('defaults');
const setupDatabase = require('../lib/db');
const setupUserModel = require('../../components/users/model');
const setupRoleModel = require('../../components/roles/model');
const setupRoleUserModel = require('../../components/role-users/model');
const setupContactModel = require('../../components/contacts/model');

////////////

const setupUser = require('../../components/users/store');
const setupRole = require('../../components/roles/store');
const setupContact = require('../../components/contacts/store');

module.exports = async (config) => {
  // console.log('config', config);
  // eslint-disable-next-line no-param-reassign
  // config = defaults(config, {
  //   dialect: 'sqlite',
  //   pool: {
  //     max: 10,
  //     min: 0,
  //     idle: 10000,
  //   },
  //   query: {
  //     raw: true,
  //   },
  // });

  const sequelize = setupDatabase(config);
  const UserModel = setupUserModel(config);
  const RoleModel = setupRoleModel(config);
  const RoleUserModel = setupRoleUserModel(config);
  const ContactModel = setupContactModel(config);

  //Relaciones
  //Users
  UserModel.belongsToMany(RoleModel, {
    through: RoleUserModel,
  });
  RoleModel.belongsToMany(UserModel, {
    through: RoleUserModel,
  });

  await sequelize.authenticate();

  if (config.setup) {
    console.log('setup');
    await sequelize.sync({
      force: true,
    });
  }

  const Users = setupUser(UserModel, RoleModel, RoleUserModel);
  const Roles = setupRole(RoleModel);
  const Contacts = setupContact(ContactModel);

  return {
    Users,
    Roles,
    Contacts,
  };
};
