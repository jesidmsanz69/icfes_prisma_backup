'use strict';

/* eslint-disable no-return-await */

const uuid = require('uuid');
const bcrypt = require('bcrypt');

module.exports = {
  up: async (queryInterface, Sequelize) => {
    const admins = [
      {
        uuid: uuid.v4(),
        username: 'abstract',
        firstName: 'Admin',
        lastName: '',
        password: bcrypt.hashSync('flo31Tr4c', 10),
        createdAt: new Date(),
        updatedAt: new Date(),
      },
    ];
    await queryInterface.bulkInsert('users', [...admins], {});

    try {
      //Admin
      const admin = admins.map(async (user) => {
        const users = await queryInterface.sequelize.query(
          `select id from users where username='${user.username}'`
        );
        const { id } = users[0][0];
        // console.log('[id]', id);

        await queryInterface.bulkInsert(
          'roleUsers',
          [{ roleId: 1, userId: id, createdAt: new Date(), updatedAt: new Date() }],
          {}
        );
      });

      await Promise.all(admin);
    } catch (error) {
      console.log('ERROR', error);
    }
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete('users', {}, {});
    await queryInterface.bulkDelete('roleUsers', {}, {});
  },
};
