require('dotenv').config();

const config = {
  hydrate: process.env.NODE_ENV === 'production' || process.env.HYDRATE === 'true' || false,
  dev: process.env.NODE_ENV !== 'production',
  port: process.env.PORT || 3000,
  cors: process.env.CORS,
  dbName: process.env.DB_NAME || 'alberto_db',
  dbUser: process.env.DB_USER || 'alberto',
  dbPassword: process.env.DB_PASSWORD || '',
  dbHost: process.env.DB_HOST || 'localhost',
  authJwtSecret: process.env.AUTH_JWT_SECRET || 'AUTH_JWT_SECRET',
  defaultAdminPassword: process.env.DEFAULT_ADMIN_PASSWORD,
  defaultUserPassword: process.env.DEFAULT_USER_PASSWORD,
  publicApiKeyToken: process.env.PUBLIC_API_KEY_TOKEN,
  adminApiKeyToken: process.env.ADMIN_API_KEY_TOKEN,
  dbAppUser: process.env.DB_APP_USER,
  dbAppPassword: process.env.DB_APP_PASSWORD,
  dbAppHost: process.env.DB_APP_HOST,
  dbAppName: process.env.DB_APP_NAME,
  dbAppPort: process.env.DB_ORACLE_PORT,
  defaultDialect: process.env.DEFAULT_DIALECT || 'sqlite',
  gmailAddress: process.env.GMAIL_ADDRESS || 'noreply8@it49.com',
  gmailOauthClientId:
    process.env.GMAIL_OAUTH_CLIENT_ID ||
    '218631959575-trgq72sovv8ptednb91ao0k41bfmppki.apps.googleusercontent.com',
  gmailOauthProjectId: process.env.GMAIL_OAUTH_PROJECT_ID || 'savvy-summit-281300',
  gmailOauthClientSecret: process.env.GMAIL_OAUTH_CLIENT_SECRET || 'wMa7JzZDh4kOlQm6jQRKOoiT',
  gmailOauthRedirectUrl: process.env.GMAIL_OAUTH_REDIRECT_URL || 'http://localhost:3000/api/smtp',
  gmailOauthRefreshToken:
    process.env.GMAIL_OAUTH_REFRESH_TOKEN ||
    '1//05911abGIOcp2CgYIARAAGAUSNwF-L9IrPmMKyMZ3Y0niJcnRsrNS37exqdKxtlDIEczSkoDAXhvyNP0e4uYjSTO-SioZSKLWbZk',
  gmailOauthAccessToken:
    process.env.GMAIL_OAUTH_ACCESS_TOKEN ||
    'ya29.a0AfH6SMBFDqikwXikdhCB-pntSe68FOTQIzgNHv-reJ7D6Ag2Nvn-uD0fBkQCRLO4NPAkaZqUxx3lhX5I07WD1EsvsJXWu03il0r1dcZp2UgYjbsVvQD7GMhSRNEqpbKZ4ntYkkT5WtAhyEqXLCCD5QAfeeRfSipbKNg',
  gmailOauthTokenExpire: process.env.GMAIL_OAUTH_TOKEN_EXPIRE || '1595437124356',
};

module.exports = { config };
