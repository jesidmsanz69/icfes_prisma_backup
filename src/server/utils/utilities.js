const NAME_COMPANY = 'Abstract 1330';
const LICENSE_COMPANY = 'Company License';
const ADDRESS_COMPANY = '1330 Federal Ave';
const CITY_COMPANY = 'Los Angeles';
const STATE_COMPANY = 'CA';
const ZIP_CODE_COMPANY = '90025';
const PHONE_COMPANY = '4243691035';
const WHATSAPP_LINK = 'https://wa.link/b0ived';
const EMAIL_COMPANY = 'robert@sternmanagement.com';
const EMAIL_CONTACT = 'robert@sternmanagement.com';
const DOMAIN_WEBSITE = 'https://abstract1330.com/';
const IP_SERVER = '8.8.8.8';

const recaptcha = require('./recaptcha');

function formatPhoneText(text) {
  let regexObj = /^(?:\+?1[-. ]?)?\(?([0-9]{3})\)?$/;
  let result = '';
  if (regexObj.test(text)) {
    result = text.replace(regexObj, '($1)');
  } else {
    regexObj = /^(?:\+?1[-. ]?)?\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})?$/;
    if (regexObj.test(text)) {
      result = text.replace(regexObj, '($1) $2-$3');
    }
  }
  return result;
}

function formatDateTime(date) {
  const options = {
    year: 'numeric',
    month: 'numeric',
    day: 'numeric',
    hour: 'numeric',
    minute: 'numeric',
    second: 'numeric',
    hour12: false,
    timeZone: 'America/Los_Angeles',
  };
  console.log('date', date);
  let finalDate = date;
  if (!(finalDate instanceof Date)) {
    finalDate = new Date(date);
  }
  console.log('finalDate', finalDate);
  return new Intl.DateTimeFormat('en-US', options).format(finalDate);
}

function getIpAddress(req) {
  let ip =
    (req.headers['x-forwarded-for'] || '').split(',')[0] ||
    req.connection.remoteAddress ||
    req.socket.remoteAddress ||
    req.connection.socket.remoteAddress;
  if (ip && ip.substr(0, 7) === '::ffff:') {
    ip = ip.substr(7);
  }
  return ip;
}

function isValidPhoneNumber(text) {
  const phoneRegex = /^(\+\d+)?\s?\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
  if (text.match(phoneRegex)) {
    return true;
  }

  return false;
}

function isValidEmail(text) {
  const regex = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  if (text.match(regex)) {
    return true;
  }

  return false;
}

function isValidName(text) {
  const regex = /^[a-zA-Z0-9 ]{2,50}$/;
  if (text.match(regex)) {
    return true;
  }

  return false;
}

module.exports = {
  ADDRESS_COMPANY,
  NAME_COMPANY,
  STATE_COMPANY,
  CITY_COMPANY,
  ZIP_CODE_COMPANY,
  EMAIL_COMPANY,
  PHONE_COMPANY,
  EMAIL_CONTACT,
  DOMAIN_WEBSITE,
  WHATSAPP_LINK,
  LICENSE_COMPANY,
  IP_SERVER,
  formatPhoneText,
  recaptcha,
  formatDateTime,
  getIpAddress,
  isValidPhoneNumber,
  isValidEmail,
  isValidName,
};
