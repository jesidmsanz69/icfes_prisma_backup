const { default: Axios } = require('axios');

const PUBLIC_KEY = '6LchL9IaAAAAAMgF1kPm59SSXHZhlNWDY_PuY57H';
const SECRET_KEY = '6LchL9IaAAAAACAR5YehLJ531cmbXmMK63VY20iV';

async function validate(response) {
  const url = `https://www.google.com/recaptcha/api/siteverify?secret=${SECRET_KEY}&response=${response}`;
  const json = await Axios.get(url);
  // const url = 'https://www.google.com/recaptcha/api/siteverify';
  // const json = await Axios.post(url, { secret: SECRET_KEY, response });
  // console.log('validate captcha json', json.data);
  // console.log('json.success', json.data.success);
  return json.data.success;
}

module.exports = {
  PUBLIC_KEY,
  SECRET_KEY,
  validate,
};
