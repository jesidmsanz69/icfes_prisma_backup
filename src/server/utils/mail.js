const nodemailer = require('nodemailer');

const { config } = require('../config');

const transporter = nodemailer.createTransport({
  // service: 'gmail',
  // auth: {
  //   user: 'noreply@gmail.com',
  //   pass: 'NoReply123.',
  // },
  host: 'smtp.gmail.com',
  port: 465,
  secure: true,
  auth: {
    type: 'OAuth2',
    user: config.gmailAddress,
    clientId: config.gmailOauthClientId,
    clientSecret: config.gmailOauthClientSecret,
    refreshToken: config.gmailOauthRefreshToken,
    accessToken: config.gmailOauthAccessToken,
    expires: Number.parseInt(config.gmailOauthTokenExpire, 10),
  },
});

function sendMail(fromName, to, replyTo, subject, body) {
  return new Promise(async (resolve, reject) => {
    const mailOptions = {
      from: `"${fromName}" <${replyTo}>`, // sender address
      to, // list of receivers
      replyTo,
      subject, // Subject line
      html: body, // plain text body
    };

    transporter.sendMail(mailOptions, function(err, info) {
      if (err) reject(err);
      else resolve(info);
    });
  });
}

module.exports = { sendMail };
