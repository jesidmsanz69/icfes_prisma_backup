const boom = require('@hapi/boom');
const { config } = require('../../config');
const controller = require('../../components/errors/controller');

function withErrorStack(error, stack) {
  if (config.dev) {
    return { ...error, stack };
  }

  return error;
}

async function logErrors(err, req, res, next) {
  console.log(err);
  try {
    await controller.create(req, res, err);
  } catch (e) {
    console.log('Error APP IT49', e);
  }
  next(err);
}

function wrapErrors(err, req, res, next) {
  if (!err.isBoom) {
    next(boom.badImplementation(err));
  }

  next(err);
}

function errorHandler(err, req, res, next) {
  // eslint-disable-line
  const {
    output: { statusCode, payload },
  } = err;
  res.status(statusCode);
  res.json(withErrorStack(payload, err.stack));
}

module.exports = {
  logErrors,
  wrapErrors,
  errorHandler,
};
