/* eslint-disable global-require */
import express from 'express';
import http from 'http';
import dotenv from 'dotenv';
import helmet from 'helmet';
import cors from 'cors';
import passport from 'passport';
// eslint-disable-next-line import/no-extraneous-dependencies
import bodyParser from 'body-parser';
import cookieParser from 'cookie-parser';
// import socket from './utils/socket';
//rutas del cliente
import main from './routes/main';
//rutas de la API
import routes from './network/routes';

const { logErrors, wrapErrors, errorHandler } = require('./utils/middleware/errorHandlers.js');

const notFoundHandler = require('./utils/middleware/notFoundHandler');
const redirectHandler = require('./utils/middleware/redirectHandler');

dotenv.config();

const ENV = process.env.NODE_ENV;
const PORT = process.env.PORT || 3000;

const app = express();
const server = http.Server(app);
// socket.connect(server);
app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(passport.initialize());
app.use(passport.session());

//Redirects
app.use(redirectHandler);

//app.use('/api', routes);
routes(app);

app.use(express.static(`${__dirname}/public`));

if (ENV === 'development') {
  console.log('Loading dev config');
  const webpackConfig = require('../../webpack.config');
  const webpack = require('webpack');
  const webpackDevMiddleware = require('webpack-dev-middleware');
  const webpackHotMiddleware = require('webpack-hot-middleware');
  const compiler = webpack(webpackConfig);
  const serverConfig = {
    port: PORT,
    hot: true,
    logLevel: 'error',
    publicPath: '/',
    writeToDisk(filePath) {
      // console.log('filePath', filePath);
      // console.log('public test', /public/.test(filePath));
      return /public/.test(filePath) || /loadable-stats/.test(filePath);
    },
  };
  app.use(webpackDevMiddleware(compiler, serverConfig));
  app.use(webpackHotMiddleware(compiler));
} else {
  // app.use(express.static(`${__dirname}/public`));
  console.log(`Loading ${ENV} config`);
  app.use(helmet());
  app.use(helmet.permittedCrossDomainPolicies());
  app.disable('x-powered-by');
}

app.use(express.static(`${__dirname}/../frontend/public`));
app.get('*', main);

// Catch 404
app.use(notFoundHandler);

// Errors middleware
app.use(logErrors);
app.use(wrapErrors);
app.use(errorHandler);

server.listen(PORT, (err) => {
  if (err) console.log(err);
  console.log(`Server running on http://localhost:${PORT}`);
});
