/* eslint-disable no-param-reassign */
/* eslint-disable space-before-function-paren */
const Sequelize = require('sequelize');
const setupDatabase = require('../../db/lib/db.js');
//
// Creating our Model
//Set it as export because we will need it required on the server
module.exports = function(config) {
  const sequelize = setupDatabase(config);
  const Model = sequelize.define('basics', {
    name: {
      type: Sequelize.STRING(50),
      allowNull: false,
      comment: 'Name',
    },

    // Timestamps
    createdAt: Sequelize.DATE,
    updatedAt: Sequelize.DATE,
  });
  return Model;
};
