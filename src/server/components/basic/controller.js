const db = require('../../db/index.js');

function findAll() {
  return new Promise(async (resolve, reject) => {
    const { Basics } = await db();

    const result = await Basics.findAll();
    resolve(result);
  });
}

function findAllActive() {
  return new Promise(async (resolve, reject) => {
    try {
      const { Basics } = await db();
      const result = await Basics.findAllActive();
      resolve(result);
    } catch (error) {
      reject(error);
    }
  });
}

function findById(id) {
  return new Promise(async (resolve, reject) => {
    const { Basics } = await db();

    const result = await Basics.findById(id);
    resolve(result);
  });
}

function deleteById(id) {
  return new Promise(async (resolve, reject) => {
    try {
      const { Basics } = await db();

      const result = await Basics.deleteById(id);
      resolve(result);
    } catch (err) {
      reject(err);
    }
  });
}

function create({ name }) {
  return new Promise(async (resolve, reject) => {
    try {
      const model = {
        name,
      };
      const { Basics } = await db();
      const result = await Basics.create(model);
      resolve(result);
    } catch (err) {
      reject(err);
    }
  });
}

function update(id, { name }) {
  return new Promise(async (resolve, reject) => {
    try {
      const model = {
        name,
      };
      const { Basics } = await db();
      const result = await Basics.update(id, model);
      resolve(result);
    } catch (err) {
      reject(err);
    }
  });
}

module.exports = {
  findAll,
  findAllActive,
  create,
  findById,
  deleteById,
  update,
};
