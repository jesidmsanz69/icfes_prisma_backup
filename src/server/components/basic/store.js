'use strict';

module.exports = function setupCountry(Model) {
  function findById(id) {
    return Model.findByPk(id);
  }
  function deleteById(id) {
    return Model.destroy({
      where: { id },
    });
  }

  function findAll() {
    return Model.findAll({ order: [['id', 'DESC']] });
  }

  function findAllActive() {
    return Model.findAll({ where: { active: true }, order: [['name', 'ASC']] });
  }

  async function create(model) {
    const result = await Model.create(model);
    return result.toJSON();
  }

  async function update(id, model) {
    const cond = { where: { id } };
    const result = await Model.update(model, cond);
    return result ? Model.findOne(cond) : false;
  }

  return {
    findById,
    findAll,
    findAllActive,
    create,
    deleteById,
    update,
  };
};
