/* eslint-disable no-param-reassign */
/* eslint-disable space-before-function-paren */
const bcrypt = require('bcrypt');
const Sequelize = require('sequelize');
const setupDatabase = require('../../db/lib/db.js');
//
// Creating our User model
//Set it as export because we will need it required on the server
module.exports = function(config) {
  const sequelize = setupDatabase(config);
  const User = sequelize.define('users', {
    uuid: {
      type: Sequelize.STRING(255),
      allowNull: false,
      comment: 'Code',
    },
    username: {
      type: Sequelize.STRING(50),
      allowNull: false,
      comment: 'Username',
    },
    // The password cannot be null
    password: {
      type: Sequelize.STRING(255),
      allowNull: false,
      comment: 'Password',
    },
    firstName: {
      type: Sequelize.STRING(50),
      allowNull: false,
      comment: 'firstName',
    },
    lastName: {
      type: Sequelize.STRING(50),
      allowNull: true,
      comment: 'lastName',
    },
    // statusId: {
    //   type: Sequelize.INTEGER,
    //   allowNull: false,
    //   comment: 'Estado',
    //   default: 1,
    // },
    createdAt: {
      type: Sequelize.DATE,
      allowNull: true,
      comment: 'created At',
    },
    updatedAt: {
      type: Sequelize.DATE,
      allowNull: true,
      comment: 'updated At',
    },
    // createdBy: {
    //   type: Sequelize.INTEGER,
    //   allowNull: true,
    //   comment: 'created By',
    // },
    // updatedBy: {
    //   type: Sequelize.INTEGER,
    //   allowNull: true,
    //   comment: 'updated By',
    // },
  });
  // Creating a custom method for our User model.
  //This will check if an unhashed password entered by the
  //user can be compared to the hashed password stored in our database
  User.prototype.validPassword = function(password) {
    return bcrypt.compareSync(password, this.password);
  };

  // Hooks are automatic methods that run during various phases of the User Model lifecycle
  // In this case, before a User is created, we will automatically hash their password
  User.beforeCreate(function(user) {
    user.password = bcrypt.hashSync(user.password, bcrypt.genSaltSync(10), null);
  });

  // Users.associate = (models) => {
  //   Users.belongsToMany(models.Roles, {
  //     through: 'RoleUsers',
  //     as: 'roles',
  //     foreignKey: 'userId',
  //   });
  // };

  return User;
};
