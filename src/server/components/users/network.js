const express = require('express');
const passport = require('passport');
const boom = require('@hapi/boom');
const jwt = require('jsonwebtoken');
const controller = require('./controller');
const response = require('../../network/response');
const { config } = require('../../config');
// import { upload } from '../../utils/files';

const router = express.Router();

// Basic strategy
require('../../utils/auth/strategies/basic');

//POST: /api/users/sign-in
router.post('/sign-in', async function(req, res, next) {
  passport.authenticate('basic', function(error, user) {
    try {
      if (error || !user) {
        next(boom.unauthorized());
        console.log('error', error);
        console.log('user', user);
        // response.error(req, res, 'El usuario y/o la contraseña son incorrectos', 401, error);
        // return;
      }

      req.login(user, { session: false }, async function(error) {
        if (error) {
          next(error);
          // return;
          // response.error(req, res, error, 401, error);
        }

        const { _id: id, name, username } = user;

        const payload = {
          sub: id,
          name,
          username,
        };

        const sessionExpire = '1d';

        const token = jwt.sign(payload, config.authJwtSecret, {
          expiresIn: sessionExpire,
        });

        const userResult = {
          ...user,
          token,
        };

        response.success(req, res, { user: userResult });
        // return res.status(200).json({ user: userResult });
      });
    } catch (error) {
      next(error);
    }
  })(req, res, next);
});
// GET: api/users
router.get(
  '/',
  // passport.authenticate('jwt', { session: false }),
  async function(req, res) {
    const result = await controller.findAll();
    response.success(req, res, result);
  }
);

// POST: api/users
router.post('/', async function(req, res) {
  try {
    const user = await controller.create(req.body);
    response.success(req, res, user);
  } catch (error) {
    console.log('ERROR: ', error);
    response.error(req, res, 'Error al registrar el usuario', 400, error);
  }
});

// POST: api/users/change-password
router.post('/change-password', passport.authenticate('jwt', { session: false }), async function(
  req,
  res
) {
  try {
    req.body.id = req.user.id;
    const result = await controller.changePassword(req.body);
    response.success(req, res, result);
  } catch (error) {
    console.log('ERROR: ', error);
    response.error(req, res, 'Error al registrar el usuario', 400, error);
  }
});

// POST: api/users/reset-password
router.post('/reset-password', passport.authenticate('jwt', { session: false }), async function(
  req,
  res
) {
  try {
    req.body.id = req.user.id;
    const result = await controller.resetPassword(req.body);
    response.success(req, res, result);
  } catch (error) {
    console.log('ERROR: ', error);
    response.error(req, res, 'Error al registrar el usuario', 400, error);
  }
});

router.post(
  '/statusChange',
  passport.authenticate('jwt', { session: false, failureRedirect: '/login' }),
  async function(req, res) {
    try {
      const { user } = req;
      const model = await controller.statusChange(req.body, user);
      response.success(req, res, model);
    } catch (error) {
      console.log('ERROR: ', error);
      response.error(req, res, 'Error al cambiar estado al usuario', 400, error);
    }
  }
);

module.exports = router;
