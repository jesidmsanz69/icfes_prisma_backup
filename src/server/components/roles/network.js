import passport from 'passport';

const express = require('express');
const controller = require('./controller');
const response = require('../../network/response');

const router = express.Router();

router.get('/', passport.authenticate('jwt', { session: false }), async function (req, res) {
  try {
    const result = await controller.findAll();
    response.success(req, res, result);
  } catch (error) {
    console.log('ERROR: ', error);
    response.error(req, res, 'Error al consultar los roles', 400, error);
  }
});

router.post('/', passport.authenticate('jwt', { session: false }), async function (req, res) {
  try {
    const { name, description } = req.body;
    const role = await controller.create({ name, description });
    response.success(req, res, role);
  } catch (error) {
    console.log('ERROR: ', error);
    response.error(req, res, 'Error al registrar el rol', 400, error);
  }
});

module.exports = router;
