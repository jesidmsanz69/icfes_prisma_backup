const db = require('../../db/index.js');

function create({ name, description }) {
  return new Promise(async (resolve, reject) => {
    const role = {
      name,
      description,
    };
    console.log('[db]', db);
    const { Roles } = await db();
    const result = await Roles.create(role);
    resolve(result);
  });
}

function findAll() {
  return new Promise(async (resolve, reject) => {
    const { Roles } = await db();

    const result = await Roles.findAll();
    resolve(result);
  });
}

module.exports = {
  create,
  findAll,
};
