const db = require('../../db/index.js');
const mail = require('../../utils/mail.js');
// const utilities = require('../../../frontend/utils/utilities.js');
const {
  recaptcha,
  EMAIL_CONTACT,
  DOMAIN_WEBSITE,
  EMAIL_COMPANY,
  NAME_COMPANY,
  isValidName,
  isValidEmail,
  isValidPhoneNumber,
} = require('../../utils/utilities');

function findAll() {
  return new Promise(async (resolve, reject) => {
    const { Contacts } = await db();

    const result = await Contacts.findAll();
    resolve(result);
  });
}

function findById(id) {
  return new Promise(async (resolve, reject) => {
    const { Contacts } = await db();

    const result = await Contacts.findById(id);
    resolve(result);
  });
}

function deleteById(id) {
  return new Promise(async (resolve, reject) => {
    const { Contacts } = await db();

    const result = await Contacts.deleteById(id);
    resolve(result);
  });
}

function validateContact({ firstName, email, phone }) {
  if (isValidName(firstName) && isValidEmail(email) && isValidPhoneNumber(phone)) {
    return true;
  }

  return false;
}

function create({ firstName, lastName, email, phone, message, ipAddress, token, sendInformation }) {
  return new Promise(async (resolve, reject) => {
    try {
      const isValidCaptcha = await recaptcha.validate(token);
      if (isValidCaptcha) {
        const model = {
          firstName,
          lastName,
          email,
          phone,
          message,
          ipAddress,
          sendInformation,
        };
        const isValidContact = validateContact(model);
        if (isValidContact) {
          resolve(true);
        } else {
          //Atack
          resolve({ id: -1 });
        }
      } else {
        reject(new Error('Captcha error'));
      }
    } catch (err) {
      // console.log('err', err);
      // try {
      //   await appError.create(req, res, err);
      // } catch (e) {}
      reject(err);
    }
  });
}

module.exports = {
  findAll,
  create,
  findById,
  deleteById,
};
