import passport from 'passport';

const express = require('express');
const controller = require('./controller');
const response = require('../../network/response');

const router = express.Router();

router.get('/',/*  passport.authenticate('jwt', { session: false }),  */async function(req, res) {
  try {
    console.log('holaaaaaaaaaaaaaaaaaa');
    const result = await controller.findAll();
    response.success(req, res, result);
  } catch (error) {
    console.log('ERROR: ', error);
    response.error(req, res, 'Error al consultar los paises', 400, error);
  }
});

router.get('/:id', passport.authenticate('jwt', { session: false }), async function(req, res) {
  try {
    const result = await controller.findById(req.params.id);
    response.success(req, res, result);
  } catch (error) {
    console.log('ERROR: ', error);
    response.error(req, res, 'Error al consultar los paises', 400, error);
  }
});

// POST: api/contacts
router.post('/', async function(req, res) {
  try {
    const ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
    req.body.ipAddress = ip;
    const model = await controller.create(req.body);
    response.success(req, res, model);
  } catch (error) {
    console.log('ERROR: ', error);
    response.error(req, res, 'Error to save contact', 400, error);
  }
});

// DELETE: api/contacts
router.delete('/:id', async function(req, res) {
  try {
    console.log('delete: contacts');
    const model = await controller.deleteById(req.params.id);
    response.success(req, res, model);
  } catch (error) {
    console.log('ERROR: ', error);
    response.error(req, res, 'Error to save contact', 400, error);
  }
});

module.exports = router;
