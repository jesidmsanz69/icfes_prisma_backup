'use strict';

module.exports = function setupCountry(Model) {
  function findById(id) {
    return Model.findByPk(id);
  }
  function deleteById(id) {
    return Model.destroy({
      where: { id },
    });
  }

  function findAll() {
    return Model.findAll({ order: [['id', 'DESC']] });
  }

  async function create(model) {
    try {
      const result = await Model.create(model);
      return result.toJSON();
    } catch (error) {
      console.log('Error to create', error);
      return { error };
    }

    // return null;
  }

  return {
    findById,
    findAll,
    create,
    deleteById,
  };
};
