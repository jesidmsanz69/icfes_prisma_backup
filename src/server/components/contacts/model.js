/* eslint-disable no-param-reassign */
/* eslint-disable space-before-function-paren */
const Sequelize = require('sequelize');
const setupDatabase = require('../../db/lib/db.js');
//
// Creating our Model model
//Set it as export because we will need it required on the server
module.exports = function(config) {
  const sequelize = setupDatabase(config);
  const Model = sequelize.define('contacts', {
    firstName: {
      type: Sequelize.STRING(100),
      allowNull: false,
      comment: 'First Name',
    },
    lastName: {
      type: Sequelize.STRING(100),
      allowNull: false,
      comment: 'Last Name',
    },
    email: {
      type: Sequelize.STRING(256),
      allowNull: false,
      comment: 'Email',
    },
    phone: {
      type: Sequelize.STRING(100),
      allowNull: false,
      comment: 'Phone',
    },
    message: {
      type: Sequelize.TEXT,
      allowNull: true,
      comment: 'Message',
    },
    ipAddress: {
      type: Sequelize.STRING(20),
      allowNull: false,
      comment: 'IP Address',
    },
    sendInformation: {
      type: Sequelize.BOOLEAN,
      allowNull: false,
      comment: 'Send information and offers from the website',
    },
  });
  return Model;
};
