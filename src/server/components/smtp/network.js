// import passport from 'passport';

const express = require('express');
// const controller = require('./controller');
const response = require('../../network/response');
const getToken = require('../../utils/google-oauth2/gmail-get-tokens');

const router = express.Router();

// GET: api/smtp
router.get('/', async function(req, res) {
  try {
    // const result = await controller.findAll();
    // console.log('req', req);
    const { code } = req.query;
    const token = await getToken(code);

    // const result = 'Ok';
    response.success(req, res, token);
  } catch (error) {
    console.log('ERROR: ', error);
    response.error(req, res, 'Error smtp', 400, error);
  }
});

router.get('/get-token', async function(req, res) {
  try {
    const result = require('../../utils/google-oauth2/gmail-generate-auth-url');
    // const result = await controller.findAll();
    // const result = 'Ok';
    response.success(req, res, result);
  } catch (error) {
    console.log('ERROR: ', error);
    response.error(req, res, 'Error al consultar los paises', 400, error);
  }
});

// POST: api/smtp
// router.post('/', async function(req, res) {
//   try {
//     console.log('api/contacts');
//     const ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
//     req.body.ipAddress = ip;
//     const model = await controller.create(req.body);
//     response.success(req, res, model);
//   } catch (error) {
//     console.log('ERROR: ', error);
//     response.error(req, res, 'Error to save contact', 400, error);
//   }
// });

module.exports = router;
