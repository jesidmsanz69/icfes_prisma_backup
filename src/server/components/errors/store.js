'use strict';

module.exports = function setupCountry(Model, sequelize) {
  async function create({ message, website, websiteurl, ipserver, status, page }) {
    try {
      const result = await sequelize.query(
        'Spr_InsErrors @message=:message, @website=:website, @websiteurl=:websiteurl, @ipserver=:ipserver, @status=:status, @page=:page',
        {
          replacements: { message, website, websiteurl, ipserver, status, page },
          // type: QueryTypes.INSERT,
          raw: true,
        }
      );
      return result;
    } catch (error) {
      console.log(error);
    }

    return null;
  }

  return {
    create,
  };
};
