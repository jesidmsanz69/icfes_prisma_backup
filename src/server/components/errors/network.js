const express = require('express');
const controller = require('./controller');
const response = require('../../network/response');

const router = express.Router();

// POST: api/errors
router.post('/', async function(req, res) {
  try {
    // const ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
    // req.body.ipAddress = ip;
    const model = await controller.create(req, res, new Error('This is a test error'));
    response.success(req, res, model);
  } catch (error) {
    console.log('ERROR: ', error);
    response.error(req, res, 'Error to save error', 400, error);
  }
});

module.exports = router;
