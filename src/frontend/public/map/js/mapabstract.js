﻿var markerGroup;
var routingControl;
var marker;
var selectedMode;

var LT = 34.047657;
var LN = -118.456308;
var LTgrocery = 34.047657;
var LNgrocery = -118.456308;
var LTdining = 34.051094;
var LNdining = -118.460209;
var LTpointsinterest = 34.047657;
var LNpointsinterest = -118.456308;
var LThospitals = 34.047657;
var LNhospitals = -118.456308;
var LTrecreation = 34.047657;
var LNrecreation = -118.456308;

let zoom = 14;

var LTtheLastUsed;
var LNtheLastUsed;

const mapLayer = 'https://{s}.basemaps.cartocdn.com/rastertiles/voyager/{z}/{x}/{y}{r}.png';
const attribution =
	'&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors &copy; <a href="https://carto.com/attributions">CARTO</a>';

//Instances of arrays
var recreation = [
	['Soul Cycle', '34.053429', '-118.46315', '', '', '(310) 559-7685', '11567 Santa Monica Blvd, <br />Los Angeles, CA 90025'],
	['The Gym LA ', '34.045021', '-118.45385', '', '', '(424) 248-8496', '11567 Santa Monica Blvd, <br />Los Angeles, CA 90025'],
	['Stoner Park', '34.038471', '-118.453003', '', '', '', '1835 Stoner Ave, <br />Los Angeles, CA 90025'],
	['Equinox', '34.044998', '-118.442513', '', '', '(310) 473-1447', '1835 S Sepulveda Blvd, <br />Los Angeles, CA 90025'],
	['Santa Monica Bay Club', '34.02814', '-118.471204', '', '', '310-829-4995', '2425 Olympic Blvd, <br />Santa Monica, CA 90404'],
	['TriFit LA', '34.029576', '-118.473136', '', '', '(310) 829-2227', '2425 Colorado Ave Ste 120, <br />Santa Monica, CA 90404'],
	['yogaraj', '34.036569', '-118.450399', '', '', '(424) 299-2507', '2001 S Barrington Ave Ste 150, <br />Los Angeles, CA 90025'],
	['Red Diamond Yoga OUTSIDE', '34.024677', '-118.411039', '', '', '(310) 425-8528', '3500 Overland Ave Ste 210, <br />Los Angeles, CA 90034'],
	
	['Orange Theory', '34.054211', '-118.464447', '', '', '310-694-5656', '11661 San Vicente Blvd #103, <br />Los Angeles, CA 90049'],
	['Barrington Recreation Center', '34.06133', '-118.468855', '', '', '', '333 S Barrington Ave, <br />Los Angeles, CA 90049'],
];
var grocery = [
	['Whole Foods', '34.05362', '-118.467326', '', '', '310-826-4433', '11737 San Vicente Blvd <br />Los Angeles, CA 90049'],
	['CVS', '34.053182', '-118.470976', '', '', '(310) 440-4162', '11941 San Vicente Blvd, <br />Los Angeles, CA 90049'],
	['Ralphs', '34.044918', '-118.466961', '', '', '310-477-8746', '12057 Wilshire Blvd <br />Los Angeles, CA 90025'],
	['Target', '34.042286', '-118.458914', '', '', '(424) 559-3327', '11840 Santa Monica Blvd, <br />Los Angeles, CA 90025'],
	['Trader Joes', '34.033716', '-118.449311', '', '', '310-477-5949', '11755 W Olympic Blvd, <br />Los Angeles, CA 90064'],
	['Smart and Final', '34.036632', '-118.437576', '', '', '310-473-0344', '11221 W Pico Blvd, <br />Los Angeles, CA 90064'],
	['Trader Joes', '34.033276', '-118.480133', '', '', '310-264-2223', '2300 Wilshire Blvd #101, <br /> Santa Monica, CA 90403'],
	['Bristol Farms', '34.040251', '-118.472801', '', '', '(310) 829-3137', '3105 Wilshire Blvd, <br /> Santa Monica, CA 90403'],
	['Erewhon Market', '34.037524', '-118.475219', '', '', '(424) 433-8111', '2800 Wilshire Blvd, <br /> Santa Monica, CA 90403'],
];
var dining = [
	['Good People Coffee', '34.044692', '-118.454433', '', '', '', '11609 Santa Monica Blvd, <br /> Los Angeles, CA 90025'],
	['Bluestone Coffee ', '34.051094', '-118.460209', '', '', '(323) 926-9604', '11601 Wilshire Blvd, <br /> Los Angeles, CA 90025'],
	['Coral Tree Café', '34.05421', '-118.46379', '', '', '(310) 979-8733', '11645 San Vicente Blvd, <br /> Los Angeles, CA 90049'],
	['Pizzana', '34.053215', '-118.466037', '', '', '(310) 481-7108', '11712 San Vicente Blvd, <br /> Los Angeles, CA 90049'],
	// ['Starbucks', '34.064485', '-118.4694', '', '', '310-472-7119', '11700 Barrington Ct, <br /> Los Angeles, CA 90049'],
	['Kreation Organic Café', '34.052997', '-118.467682', '', '', '(424) 372-3087', '11754 San Vicente Blvd, <br /> Los Angeles, CA 90049'],
	['Sugarfish ', '34.053509', '-118.46264', '', '', '310-820-4477', '11640 San Vicente Blvd, <br /> Los Angeles, CA 90049'],
	['New York Bagel Company', '34.053509', '-118.46264', '', '', '310-820-1050', '11640 San Vicente Blvd, <br /> Los Angeles, CA 90049'],
	['Baltaire', '34.054217', '-118.463998', '', '', '424-273-1660', '11647 San Vicente Blvd, <br /> Los Angeles, CA 90049'],
	['Caffee Luxxe', '34.053493', '-118.462518', '', '', '(310) 394-2222', '11640 San Vicente Blvd #101, <br /> Los Angeles, CA 90049'],
	['Vincenti Ristorante', '34.052285', '-118.471204', '', '', ' (310) 207-0127', '11930 San Vicente Blvd, <br /> Los Angeles, CA 90049'],
];
var pointsinterest = [
	['Century City Mall ', '34.059003', '-118.418866', '', '', '310-277-3898', '10250 Santa Monica Blvd, <br /> Los Angeles, CA 90067'],
	['Brentwood Farmers Market', '34.050737', '-118.475767', '', '', '', '741 S Gretna Green Way, <br /> Los Angeles, CA 90049'],
	['Santa Monica Pier', '34.010957', '-118.494997', '', '', '310-458-8900', '200 Santa Monica Pier, <br /> Santa Monica, CA 90401'],
	['UCLA Campus ', '34.070264', '-118.444056', '', '', '310-825-4321', 'Los Angeles, CA 90095'],
	['Getty Museum', '34.087701', '-118.475786', '', '', ' (310) 440-7300 ', '1200 Getty Center Dr, <br /> Los Angeles, CA 90049'],
	['Will Rodgers State Park', '34.054954', '-118.512622', '', '', '(310) 230-2017', '1501 Will Rogers State Park Rd, <br /> Pacific Palisades, CA'],
	['LMU  (Layola Marymount University)', '33.968091', '-118.421362', '', '', '310-338-2700', ' Loyola Marymount University Dr, <br /> Los Angeles, CA 90045'],
	['LACMA ( Los Angeles County Museum Art)', '34.063791', '-118.358885', '', '', '323-857-6000', '5905 Wilshire Blvd, <br /> Los Angeles, CA 90036'],
	['Los Leones Trailhead  ', '34.046954', '-118.559923', '', '', 'phone', '566 Los Leones Dr.  <br />  Pacific Palisades, CA 90272 '],
];
var hospitals = [
	['Veterans Hospital  ', '34.05263', '-118.452734', '', '', '310-478-3711', '11301 Wilshire Blvd, <br /> Los Angeles, CA 90073'],
	['UCLA Hospital ', '34.066476', '-118.446574', '', '', '310-825-9111', ' 757 Westwood Plaza, <br /> Los Angeles, CA 90095'],
	['St. Johns Hospital/ Health Care Center ', '34.030774', '-118.479644', '', '', '310-829-5511', '2121 Santa Monica Blvd, <br /> Santa Monica, CA 90404'],
];


//Setting coordinates
latlng = new L.LatLng(LT, LN);
latlngpointsinterest = new L.LatLng(LTpointsinterest, LNpointsinterest);
latlnghospitals = new L.LatLng(LThospitals, LNhospitals);
latlngrecreation = new L.LatLng(LTrecreation, LNrecreation);
latlnggrocery = new L.LatLng(LTgrocery, LNgrocery);
latlngdining = new L.LatLng(LTdining, LNdining);

//Calling Map
var map = L.map('maproperty').setView(latlngrecreation, 14);
map.scrollWheelZoom.disable();
L.tileLayer(mapLayer, {
	// apiKey: apiKey,
	attribution: attribution,
}).addTo(map);

map.setView([LTrecreation, LNrecreation], zoom);

//Put Icon Marker of the property
var markerIcon = L.icon({
	iconUrl: 'images/map/marker.png',
	iconSize: [33, 32], // size of the icon
	iconAnchor: [16, 32], // point of the icon which will correspond to marker's location
	popupAnchor: [0, -32], // point from which the popup should open relative to the iconAnchor
});
var markerSource = L.marker(latlng, { icon: markerIcon }).addTo(map);
markerSource.bindPopup("<div class='contentinfowindow'><p><strong>Abstract 1330 <br /> 1330 Federal Ave  Los Angeles, CA 90025 <br /> (424) 396-1035</strong></p></div>");
markerSource.on('mouseover', function (event) {
	markerSource.openPopup();
});
markerSource.on('mouseout', function (event) {
	markerSource.closePopup();
});

//Adding Recreation Fitness by default inside the map
AddArrayToMap(recreation, 'recreation');

//Add all the links of businesses
FillListPlaces();

//Event of links category, so the map changes array markers
$('.list-group-item a').click(function () {
	if ($(this).attr('aria-controls') == 'pointsinterest') {
		AddArrayToMap(pointsinterest, 'pointsinterest');
		map.setZoom(12);
		setTimeout(function () {
			map.panTo(latlngpointsinterest);
		}, 400);
	} else if ($(this).attr('aria-controls') == 'hospitals') {
		AddArrayToMap(hospitals, 'hospitals');
		map.setZoom(13);
		setTimeout(function () {
			map.panTo(latlnghospitals);
		}, 400);
	} else if ($(this).attr('aria-controls') == 'recreation') {
		AddArrayToMap(recreation, 'recreation');
		map.setZoom(14);
		setTimeout(function () {
			map.panTo(latlngrecreation);
		}, 400);
	} else if ($(this).attr('aria-controls') == 'grocery') {
		AddArrayToMap(grocery, 'grocery');
		map.setZoom(14);
		setTimeout(function () {
			map.panTo(latlnggrocery);
		}, 400);
	} else if ($(this).attr('aria-controls') == 'dining') {
		AddArrayToMap(dining, 'dining');
		map.setZoom(14);
		setTimeout(function () {
			map.panTo(latlngdining);
		}, 400);
	}
	if (routingControl) {
		map.removeControl(routingControl);
	}
});

//Event of Select Travel Mode
document.getElementById('mode').addEventListener('change', function () {
	DrivingDirectionFromTo();
});

//Event of the links of businesses
$('.tab-pane').on('click', 'a', function () {
	LTtheLastUsed = $(this).attr('data-lat');
	LNtheLastUsed = $(this).attr('data-lon');
	DrivingDirectionFromTo();
	return false;
});

//This function fills the links that are located under the map section
function FillListPlaces() {
	$('#grocery').append("<h2>Grocery Stores</h2><ol id='listgrocery'></ol>");
	for (var i = 0; i < grocery.length; i++) {
		$('#listgrocery').append(
			"<li><a href='#' title='Click to get direction' data-lat='" +
				grocery[i][1] +
				"' data-lon='" +
				grocery[i][2] +
				"' data-url='" +
				grocery[i][4] +
				"'>" +
				grocery[i][0] +
				'</a></li>'
		);
	}
	$('#pointsinterest').append("<h2>Points of Interest</h2><ol id='listpointsinterest'></ol>");
	for (var i = 0; i < pointsinterest.length; i++) {
		$('#listpointsinterest').append(
			"<li><a href='#' title='Click to get direction' data-lat='" +
				pointsinterest[i][1] +
				"' data-lon='" +
				pointsinterest[i][2] +
				"' data-url='" +
				pointsinterest[i][4] +
				"'>" +
				pointsinterest[i][0] +
				'</a></li>'
		);
	}	
	$('#hospitals').append("<h2>Hospitals</h2><ol id='listhospitals'></ol>");
	for (var i = 0; i < hospitals.length; i++) {
		$('#listhospitals').append(
			"<li><a href='#' title='Click to get direction' data-lat='" +
				hospitals[i][1] +
				"' data-lon='" +
				hospitals[i][2] +
				"' data-url='" +
				hospitals[i][4] +
				"'>" +
				hospitals[i][0] +
				'</a></li>'
		);
	}
	$('#recreation').append("<h2>Recreation + Fitness</h2><ol id='listrecreation'></ol>");
	for (var i = 0; i < recreation.length; i++) {
		$('#listrecreation').append(
			"<li><a href='#' title='Click to get direction' data-lat='" +
				recreation[i][1] +
				"' data-lon='" +
				recreation[i][2] +
				"' data-url='" +
				recreation[i][4] +
				"'>" +
				recreation[i][0] +
				'</a></li>'
		);
	}
	$('#dining').append("<h2>Restaurants</h2><ol id='listdining'></ol>");
	for (var i = 0; i < dining.length; i++) {
		$('#listdining').append(
			"<li><a href='#' title='Click to get direction' data-lat='" +
				dining[i][1] +
				"' data-lon='" +
				dining[i][2] +
				"' data-url='" +
				dining[i][4] +
				"'>" +
				dining[i][0] +
				'</a></li>'
		);
	}
}

//This function adds an array of businesses markers to the map
function AddArrayToMap(arrayItems, category) {
	if (markerGroup) {
		map.removeLayer(markerGroup);
	}

	markerGroup = L.layerGroup().addTo(map);

	var contentString = '';

	for (var i = 0; i < arrayItems.length; i++) {
		var markerLT = arrayItems[i][1];
		var markerLN = arrayItems[i][2];

		var markerIcon = L.icon({
			iconUrl: 'images/map/marker-' + category + '.svg',
			iconSize: [30, 30],
			iconAnchor: [15, 30],
			popupAnchor: [0, -30],
		});

		var telephone="";
		if (arrayItems[i][5] !== "") {
			telephone = '<br />' +arrayItems[i][5];
		}
		var address = "";
		if (arrayItems[i][6] !== "") {
			address = '<br />' +arrayItems[i][6];
		}

		contentString =
			"<div class='contentinfowindow text-center'><p><strong>" +
			arrayItems[i][0] +
			'</strong>' +
			telephone + address
			'</p></div>';

		eval('var marker' + i + ' = L.marker([markerLT, markerLN], { icon: markerIcon }).addTo(markerGroup)');
		eval('marker' + i + '.bindPopup(contentString);');
		eval('marker' + i + ".on('mouseover', function (event) { marker" + i + '.openPopup(); });');
		eval('marker' + i + ".on('mouseout', function (event) { marker" + i + '.closePopup(); });');
		eval(
			'marker' +
				i +
				".on('click', function (event) { LTtheLastUsed=" +
				arrayItems[i][1] +
				';LNtheLastUsed=' +
				arrayItems[i][2] +
				';DrivingDirectionFromTo(); });'
		);
	}
}

//This function handles everything routing direction
function DrivingDirectionFromTo() {
	if (marker) {
		map.removeLayer(marker);
	}

	if (routingControl) {
		map.removeControl(routingControl);
	}

	selectedMode = document.getElementById('mode').value;

	routingControl = L.Routing.control({
		waypoints: [L.latLng(LT, LN), L.latLng(LTtheLastUsed, LNtheLastUsed)],
		geocoder: L.Control.Geocoder.nominatim(),
		router: L.Routing.graphHopper('b8593a31-920f-421a-8d7a-0e8d9809f6b8', {
			urlParameters: {
				vehicle: selectedMode,
			},
		}),
		routeWhileDragging: false,
		fitSelectedRoutes: true,
		defaultErrorHandler: false,
		units: 'imperial',
	}).addTo(map);

	var router = routingControl.getRouter();

	routingControl._map = map;
	var controlDiv = routingControl.onAdd(map);
	$('#paneldirections').append(controlDiv);
	$('#paneldirections').show();

	router.on('routingerror', function () {
		map.removeControl(routingControl);
		alert('No routes found');
		marker = L.marker([LT, LN]).addTo(map);
		map.setView([LT, LN], zoom);
		$('#floating-panel').hide();
	});
	router.on('response', function (e) {
		$('#floating-panel').show();
		console.log('This request consumed ' + e.credits + ' credit(s)');
		console.log('You have ' + e.remaining + ' left');
		eval(
			'setTimeout(function () { ValidateInfoBusiness(' +
				$('.list-group-item.active a').attr('href').replace('#', '') +
				') }, 1000);'
		);
	});
}

//This function shows info data of a business such us name, address, phone and website
function ValidateInfoBusiness(arrayItems) {
	var namebusiness = '';
	var addressinfobusiness = '';
	var phonebusiness = '';
	for (var i = 0; i < arrayItems.length; i++) {
		if (arrayItems[i][1] == LTtheLastUsed && arrayItems[i][2] == LNtheLastUsed) {
			namebusiness = arrayItems[i][0];
			addressinfobusiness = arrayItems[i][6];
			phonebusiness = arrayItems[i][5];
			break;
		}
	}
	var datadistanceandtime = $('.leaflet-routing-container h3').html();
	if (datadistanceandtime.indexOf('strong>') == -1) {
		if (phonebusiness.trim() != '') {
			phonebusiness = phonebusiness + '<br />';
		}
		if (addressinfobusiness.trim() != '') {
			addressinfobusiness = '<span>' + addressinfobusiness + '</span><br />';
		}
		$('.leaflet-routing-container h3').html(
			'<strong>' + namebusiness + '</strong><br />' + phonebusiness + addressinfobusiness + datadistanceandtime
		);
	}
}
