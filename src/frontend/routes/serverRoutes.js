import baseLoadable from '@loadable/component';
import React from 'react';
import loadingimg from '../assets/static/loading.svg';
// import Home from '../pages/home';
// import Services from '../pages/services';
// import Login from '../pages/Login';
// // import NotFound from '../pages/NotFound';
// import NoMatch from '../pages/no-match';
// import ChangePassword from '../pages/users/ChangePassword';
// import AdminResetPassword from '../pages/users/AdminResetPassword';
// import Projects from '../pages/projects';
// import About from '../pages/about';
// import Contact from '../pages/contact';
// import ContactsList from '../pages/admin/contacts/ContactsList';
// import ContactsDetails from '../pages/admin/contacts/ContactsDetails';

function LoadingContent() {
  return (
    <div className="contentloading">
      <img src={loadingimg} alt="Loading Content..." />
    </div>
  );
}

function loadable(func) {
  return baseLoadable(func, { fallback: <LoadingContent /> });
}

const Home = loadable(() => import('../pages/Home'));
const FloorPlan = loadable(() => import('../pages/FloorPlan'));
const PhotoGallery = loadable(() => import('../pages/PhotoGallery'));
const Amenities = loadable(() => import('../pages/Amenities'));
const MapAndDirection = loadable(() => import('../pages/MapAndDirection'));
const ContactUs = loadable(() => import('../pages/ContactUs'));
const WebAccessibility = loadable(() => import('../pages/WebAccessibility'));
const Login = loadable(() => import('../pages/Login'));
const NoMatch = loadable(() => import('../pages/no-match'));
const ChangePassword = loadable(() => import('../pages/users/ChangePassword'));
const AdminResetPassword = loadable(() => import('../pages/users/AdminResetPassword'));
const ContactsList = loadable(() => import('../pages/admin/contacts/ContactsList'));
const ContactsDetails = loadable(() => import('../pages/admin/contacts/ContactsDetails'));
const Sitemap = loadable(() => import('../pages/sitemap'));

const serverRoutes = (isLogged, redirect = true) => [
  // {
  //id: 1,
  //   path: '/',
  //   component: !isLogged && redirect ? Login : Home,
  //   exact: true,
  // },
  {
    id: 2,
    path: '/',
    component: Home,
    exact: true,
  },
  {
    path: '/floorplans',
    component: FloorPlan,
    exact: true,
  },
  {
    path: '/photogallery',
    component: PhotoGallery,
    exact: true,
  },
  {
    path: '/amenities',
    component: Amenities,
    exact: true,
  },
  {
    path: '/BrentwoodNeighborhood',
    component: MapAndDirection,
    exact: true,
  },
  {
    path: '/contactUs/:bed?/:bath?',
    component: ContactUs,
    exact: true,
  },
  {
    path: '/web-accessibility',
    component: WebAccessibility,
    exact: true,
  },
  {
    id: 3,
    path: '/sitemap',
    component: Sitemap,
    exact: true,
  },
  {
    id: 4,
    path: '/login',
    component: Login,
    exact: true,
    layout: 'none',
  },
  {
    id: 5,
    path: '/admin/change-password',
    component: !isLogged && redirect ? Login : ChangePassword,
    exact: true,
    layout: 'admin',
  },
  {
    id: 6,
    path: '/admin/reset-password',
    component: !isLogged && redirect ? Login : AdminResetPassword,
    exact: true,
    layout: 'admin',
  },
  {
    id: 7,
    path: '/admin',
    component: !isLogged && redirect ? Login : ContactsList,
    exact: true,
    layout: 'admin',
  },
  {
    id: 8,
    path: '/admin/contacts',
    component: !isLogged && redirect ? Login : ContactsList,
    exact: true,
    layout: 'admin',
  },
  {
    id: 9,
    path: '/admin/contacts/:id',
    component: !isLogged && redirect ? Login : ContactsDetails,
    exact: true,
    layout: 'admin',
  },
  {
    id: 10,
    path: '/loading',
    component: LoadingContent,
    exact: true,
  },
  { id: 11, name: 'NotFound', component: NoMatch },
];

export default serverRoutes;
