import React, { useEffect, useRef } from 'react';
import FormGroup from 'react-bootstrap/FormGroup';
import PropTypes from 'prop-types';

function CustomFormGroup(props) {
  const ref = useRef();

  useEffect(() => {
    const $label = ref.current.querySelector('.form-label');
    const $input = ref.current.querySelector('.form-control');
    if ($input && $label) {
      $input.addEventListener('focus', function() {
        $label.classList.remove('floatlabelnow');
        $label.classList.add('floatlabelnow');
      });
      $input.addEventListener('focusout', function() {
        if (!$input.value) {
          $label.classList.remove('floatlabelnow');
        }
      });
    }
  }, []);

  return (
    <FormGroup {...props} ref={ref}>
      {props.children}
    </FormGroup>
  );
}

CustomFormGroup.propTypes = {
  className: PropTypes.string,
  controlId: PropTypes.string,
};

export default CustomFormGroup;
