import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

// import { FormControl } from 'react-bootstrap';
import CurrencyFormat from 'react-currency-format';
// import { formatPhoneMask } from '../../utils/utilities';

function InputPhone(props) {
  // const handleChange = (evt) => {
  //   const event = evt;
  //   event.target.value = formatPhoneMask(event.target.value);
  //   props.onChange(event);
  // };

  // return <FormControl {...props} onChange={handleChange} />;
  return (
    <CurrencyFormat
      {...props}
      className={classNames('form-control', props.className)}
      format="+1 (###) ###-####"
      mask="_"
    />
  );
}

InputPhone.propTypes = {
  id: PropTypes.string,
  name: PropTypes.string,
  className: PropTypes.string,
  value: PropTypes.string,
  onChange: PropTypes.func,
};

export default InputPhone;
