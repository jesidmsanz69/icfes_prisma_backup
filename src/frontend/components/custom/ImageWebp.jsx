/* eslint-disable jsx-a11y/alt-text */
import React from 'react';
import PropTypes from 'prop-types';

function ImageWebp({ src, mobilesrc, webp, type = 'image/webp', ...delegated }) {
  let webpSrc = webp;
  let webpSrcMobile = webp;
  let mobilePic;
  if (!webp) {
    webpSrc = src.replace(/\.(jpe?g|png)/i, '.webp');
  }
  if (mobilesrc !== undefined) {
    webpSrcMobile = mobilesrc.replace(/\.(jpe?g|png)/i, '.webp');
    mobilePic = <source media="(max-width: 640px)" srcSet={webpSrcMobile} type={type}></source>;
  }
  return (
    <picture>
      {mobilePic}
      <source srcSet={webpSrc} type={type} />
      <img src={src} {...delegated} />
    </picture>
  );
}

ImageWebp.propTypes = {
  src: PropTypes.string,
  mobilesrc: PropTypes.string,
  webp: PropTypes.string,
  type: PropTypes.string,
};

export default ImageWebp;
