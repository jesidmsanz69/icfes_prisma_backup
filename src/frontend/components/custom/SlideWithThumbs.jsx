import React, { useState } from 'react';
import AliceCarousel from 'react-alice-carousel';
import ImageWebp from './ImageWebp';
import 'react-alice-carousel/lib/alice-carousel.css';
import '../../assets/styles/components/Gallery.scss';
import 'blueimp-gallery/css/blueimp-gallery.min.css';

const responsiveThumbs = {
  0: { items: 4 },
  568: { items: 5 },
};

const handleDragStart = (e) => e.preventDefault();

const ItemGallery = ({ index, normal, large }) => (
  <ImageWebp
    src={normal}
    alt={`vintage hollywood apartments gallery #${index}`}
    className="img-fluid imagegallery"
    onDragStart={handleDragStart}
    data-target={large}
    data-index={index}
  />
);

const thumbItems = (items, [setThumbIndex, setThumbAnimation]) => {
  return items.map((item, i) => (
    <div
      role="button"
      tabIndex="0"
      key={i}
      className="thumb"
      onClick={() => (setThumbIndex(i), setThumbAnimation(true))}
    >
      {item}
    </div>
  ));
};

const Carousel = ({ itemsparam, galleryOnClick }) => {
  const [mainIndex, setMainIndex] = useState(0);
  const [mainAnimation, setMainAnimation] = useState(false);
  const [thumbIndex, setThumbIndex] = useState(0);
  const [thumbAnimation, setThumbAnimation] = useState(false);
  const [thumbs] = useState(thumbItems(itemsparam, [setThumbIndex, setThumbAnimation]));

  const slideNext = () => {
    if (!thumbAnimation && thumbIndex < thumbs.length - 1) {
      setThumbAnimation(true);
      setThumbIndex(thumbIndex + 1);
    }
  };

  const slidePrev = () => {
    if (!thumbAnimation && thumbIndex > 0) {
      setThumbAnimation(true);
      setThumbIndex(thumbIndex - 1);
    }
  };

  const syncMainBeforeChange = (e) => {
    setMainAnimation(true);
    if (e.type === 'action') {
      setThumbAnimation(true);
    }
  };

  const syncMainAfterChange = (e) => {
    setMainAnimation(false);

    if (e.type === 'action') {
      setThumbIndex(e.item);
      setThumbAnimation(false);
    } else {
      setMainIndex(thumbIndex);
    }
  };

  const syncThumbs = (e) => {
    setThumbIndex(e.item);
    setThumbAnimation(false);

    if (!mainAnimation) {
      setMainIndex(e.item);
    }
  };

  return (
    <>
      <div onClick={galleryOnClick} className="modelarge" role="button" tabIndex="0">
        <AliceCarousel
          activeIndex={mainIndex}
          animationType="fadeout"
          animationDuration={100}
          disableDotsControls
          disableButtonsControls
          disableSlideInfo={false}
          infinite
          items={itemsparam}
          mouseTracking={!thumbAnimation}
          onSlideChange={syncMainBeforeChange}
          onSlideChanged={syncMainAfterChange}
          touchTracking={!thumbAnimation}
        />
      </div>
      <div className="thumbs">
        <AliceCarousel
          activeIndex={thumbIndex}
          disableButtonsControls
          items={thumbs}
          mouseTracking={false}
          onSlideChanged={syncThumbs}
          touchTracking={!mainAnimation}
          responsive={responsiveThumbs}
        />
      </div>
      <div className="btn-prev" onClick={slidePrev} role="button" tabIndex="0">
        &lang;
      </div>
      <div className="btn-next" onClick={slideNext} role="button" tabIndex="0">
        &rang;
      </div>
    </>
  );
};

class SlideWithThumbs extends React.Component {
  constructor(props) {
    super(props);
    this.items = [];
    this.galleryRef = React.createRef();
    this.linksGalleryArrray = [];
    this.valuesNormal = Object.keys(props.listnormal).map((itm) => props.listnormal[itm]);
    this.valuesLarge = Object.keys(props.listlarge).map((itm) => props.listlarge[itm]);
    for (let i = 0; i < this.valuesNormal.length; i++) {
      this.items.push(
        <ItemGallery
          key={this.valuesNormal[i]}
          index={i}
          normal={this.valuesNormal[i]}
          large={this.valuesLarge[i]}
        />
      );
    }
    for (let i = 0; i < this.valuesNormal.length; i++) {
      this.linksGalleryArrray.push(this.valuesLarge[i]);
    }
    this.goBlueimp = this.goBlueimp.bind(this);
  }

  goBlueimp(evt) {
    const Gallery = require('blueimp-gallery');
    let event = evt;
    event = event || window.event;
    const target = event.target || event.srcElement,
      link = target.dataset.index,
      options = { index: link, event };
    console.log('hellooooo--->', link);
    console.log('hellooooo--->', this.linksGalleryArrray);

    if (target.className.toLowerCase().indexOf('imagegallery') !== -1) {
      console.log('link :>> ', link);
      Gallery(this.linksGalleryArrray, { index: parseInt(link) });
    }
  }

  render() {
    return (
      <div className="carouselgallery">
        <Carousel galleryOnClick={this.goBlueimp} itemsparam={this.items} />
      </div>
    );
  }
}

export default SlideWithThumbs;
