import React from 'react';
import loadingImg from '../../assets/static/loading.svg';

function Loading() {
  return (
    <div className="contentloading text-center">
      <img src={loadingImg} alt="Loading Content..." />
    </div>
  );
}

export default Loading;
