import React from 'react';
import { Row, Col, Form } from 'react-bootstrap';
import axios from 'axios';
// import ReCAPTCHA from 'react-google-recaptcha';
import { ReCaptcha, loadReCaptcha } from 'react-recaptcha-v3';
import MessageBox from './MessageBox';
import { EMAIL_CONTACT, NAME_COMPANY, recaptcha } from '../utils/utilities';
import CustomFormGroup from './custom/CustomFormGroup';
import { createBodyEmail, sendEmail } from '../utils/apiEmails';

class FormContact extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      validated: false,
      form: {
        firstName: '',
        lastName: '',
        email: '',
        phone: '',
        message: '',
        sendInformation: '',
        property: ` Bed: ${this.props.bed} Bath: ${this.props.bath}`,
        token: '',
      },
      loading: false,
      success: false,
      error: false,
      captcha: true,
    };
    this.recaptchaRef = React.createRef();
    this.isLoadedRecaptcha = false;

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleChangeCaptcha = this.handleChangeCaptcha.bind(this);
    this.onCaptchaExpired = this.onCaptchaExpired.bind(this);
    this.verifyCallback = this.verifyCallback.bind(this);
    this.updateToken = this.updateToken.bind(this);
    this.handleChangeRadio = this.handleChangeRadio.bind(this);
    this.loadCaptcha = this.loadCaptcha.bind(this);
  }

  verifyCallback(token) {
    // console.log('token', token);
    const { form } = this.state;
    this.setState({
      form: {
        ...form,
        token,
        captcha: !!token,
      },
    });
  }

  loadCaptcha() {
    if (!this.isLoadedRecaptcha) {
      loadReCaptcha(recaptcha.PUBLIC_KEY, (res) => console.log('loadReCaptcha', res));
      this.isLoadedRecaptcha = true;
    }
  }

  updateToken() {
    this.recaptchaRef.execute();
  }

  handleChange(event) {
    this.loadCaptcha();
    const { form } = this.state;
    // console.log('event', event);
    this.setState({
      form: {
        ...form,
        [event.target.name]: event.target.files ? event.target.files : event.target.value,
      },
    });
  }

  handleChangeRadio(value1, value) {
    this.loadCaptcha();
    const { form } = this.state;
    this.setState({
      form: {
        ...form,
        sendInformation: value,
      },
    });
  }

  handleChangeCaptcha(value) {
    console.log('Captcha value:', value);
    this.state.setState({ captcha: true });
  }

  onCaptchaExpired() {
    this.state.setState({ captcha: false });
  }

  handleSubmit(event) {
    event.preventDefault();
    this.loadCaptcha();
    const formControl = event.currentTarget;
    const { form } = this.state;
    const { token } = form;
    if (!token) {
      this.updateToken();
      this.setState({ captcha: false, validated: true });
    } else if (formControl.checkValidity() !== false) {
      this.setState({ loading: true });
      axios
        .post('contacts', form)
        .then(({ data }) => {
          if (data) {
            this.setState({ success: true, loading: false });
            if (form.sendInformation) {
              try {
                let clientEmail = EMAIL_CONTACT;
                const fullName = `${form.firstName} ${form.lastName}`
                const formName = 'Form Contact';
                const subjectEmail = `${fullName} - ${formName}`;
                const body = createBodyEmail(`${fullName}`, form.email, form.phone, form.message, form.sendInformation);

                //Our Client
                sendEmail(`${fullName}`, clientEmail, form.email, `${NAME_COMPANY} -` + subjectEmail, body);
                //User who filled the form
                sendEmail(`${fullName}`, form.email, clientEmail, `${NAME_COMPANY} -` + subjectEmail + " (Confirmation Email)", body, true);

              } catch (error) {
                console.log('error', error)
              }
            }
          }
          else this.setState({ error: true, loading: false });
        })
        .catch((err) => {
          console.log('err', err);
          this.setState({ error: true, loading: false });
          this.updateToken();
        });
    } else {
      this.setState({ validated: true });
    }
  }

  render() {
    if (this.state.success) {
      return (
        <MessageBox
          type={1}
          text="We have received your request. Thank you!"
        // text="We have received your request, in a moment you will receive a confirmation email. Thank you!"
        />
      );
    }

    const { form } = this.state;

    return (
      <Form
        noValidate
        validated={this.state.validated}
        onSubmit={this.handleSubmit}
        action="/quote-submit"
      >
        <h3 className="h3">How can we help you?</h3>
        <Row className="mt-4">
          <Col sm={6}>
            <CustomFormGroup className="form-floating" controlId="txtFirstName">
              <Form.Label>First name</Form.Label>
              <Form.Control
                required
                type="text"
                maxLength="30"
                name="firstName"
                value={this.state.form.firstName}
                onChange={this.handleChange}
                placeholder="First name*"
              />
              <Form.Control.Feedback type="invalid">
                Please provide your first name
              </Form.Control.Feedback>
            </CustomFormGroup>
          </Col>
          <Col sm={6}>
            <CustomFormGroup className="form-floating" controlId="txtLastName">
              <Form.Label>Last name</Form.Label>
              <Form.Control
                required
                type="text"
                maxLength="30"
                name="lastName"
                value={this.state.form.lastName}
                onChange={this.handleChange}
                placeholder="Last name*"
              />
              <Form.Control.Feedback type="invalid">
                Please provide your last name
              </Form.Control.Feedback>
            </CustomFormGroup>
          </Col>
        </Row>
        <Row>
          <Col sm={6}>
            <CustomFormGroup className="form-floating" controlId="txtEmail">
              <Form.Label>Email</Form.Label>
              <Form.Control
                required
                type="email"
                maxLength="256"
                name="email"
                value={this.state.form.email}
                onChange={this.handleChange}
                placeholder="Email*"
              />
              <Form.Control.Feedback type="invalid">
                Please provide a valid email
              </Form.Control.Feedback>
            </CustomFormGroup>
          </Col>
          <Col sm={6}>
            <CustomFormGroup className="form-floating" controlId="txtPhone">
              <Form.Label>Phone Number</Form.Label>
              <Form.Control
                required
                type="tel"
                maxLength="20"
                name="phone"
                value={this.state.form.phone}
                onChange={this.handleChange}
                placeholder="Phone Number*"
              />
              <Form.Control.Feedback type="invalid">
                Please provide a valid telephone
              </Form.Control.Feedback>
            </CustomFormGroup>
          </Col>
        </Row>
        {this.props.bed && this.props.bath && (
          <CustomFormGroup className="form-floating" controlId="property">
            <Form.Label className="floatlabelnow">Property</Form.Label>
            <Form.Control
              type="tel"
              maxLength="20"
              name="property"
              value={this.state.form.property}
              onChange={this.handleChange}
              placeholder="Property"
              disabled
            />
          </CustomFormGroup>
        )}
        <CustomFormGroup className="form-floating" controlId="txtMesageContact">
          <Form.Label>Message</Form.Label>
          <Form.Control
            required
            as="textarea"
            rows="5"
            maxLength="500"
            name="message"
            value={this.state.form.message}
            onChange={this.handleChange}
            placeholder="Message*"
          />
          <Form.Control.Feedback type="invalid">Please type your message</Form.Control.Feedback>
        </CustomFormGroup>
        <fieldset className="form-group">
          <legend>
            Do you want to receive email notifications, news, <br /> and specials from us?
          </legend>
          <div className="form-check form-check-inline">
            <input
              className="form-check-input"
              type="radio"
              name="sendInformation"
              value={this.state.form.sendInformation}
              id="sendInformation1"
              onChange={(event) => this.setState({
                form: {
                  ...form,
                  sendInformation: true,
                },
              })}
              required
            />
            <label className="form-check-label" htmlFor="sendInformation1">
              Yes
              <span className="sr-only">you want to receive email notifications</span>
            </label>
          </div>
          <div className="form-check form-check-inline">
            <input
              className="form-check-input"
              type="radio"
              name="sendInformation"
              value={this.state.form.sendInformation}
              id="sendInformation2"
              onChange={(event) => this.setState({
                form: {
                  ...form,
                  sendInformation: true,
                },
              })}
            />
            <label className="form-check-label" htmlFor="sendInformation2">
              No
              <span className="sr-only">you don't want to receive email notifications</span>
            </label>
          </div>
          {this.state.validated && this.state.form.sendInformation === '' && (
            <span className="invalid-feedback d-block">Please, select yes or no</span>
          )}
        </fieldset>
        <CustomFormGroup>
          <ReCaptcha
            ref={(ref) => (this.recaptchaRef = ref)}
            sitekey={recaptcha.PUBLIC_KEY}
            action="submit"
            verifyCallback={this.verifyCallback}
          />
          {!this.state.captcha && (
            <span className="invalid-feedback d-block">Captcha is required</span>
          )}
        </CustomFormGroup>

        {this.state.error && (
          <MessageBox type={3} text="Has occurred an error while saving, please try again!" />
        )}
        <p className="mb-0 text-center">
          <button
            type="submit"
            // id="button-contact"
            className="btn btn-primary mb-3"
            // variant="warning"
            disabled={this.state.loading}
          >
            {this.state.loading ? 'Loading...' : 'SEND MY MESSAGE'}
            {/* <span> </span> */}
          </button>
        </p>
      </Form>
    );
  }
}

export default FormContact;
