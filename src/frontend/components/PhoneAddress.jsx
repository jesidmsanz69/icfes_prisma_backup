import React from 'react';

import location from '../assets/static/images/icons/location.svg';
import call from '../assets/static/images/icons/call.svg';
import {
  PHONE_COMPANY,
  ADDRESS_COMPANY,
  CITY_COMPANY,
  STATE_COMPANY,
  ZIP_CODE_COMPANY,
  formatPhoneText,
} from '../../server/utils/utilities';

export default function PhoneAddress() {
  return (
    <>
      <div className="d-flex align-items-start mb-3">
        <img src={location} alt="Location" className="me-2" />
        <a
          target="_blank"
          href="http://maps.google.com?q=1330+Federal+Ave++Los+Angeles+CA+90025"
          rel="noreferrer"
          className="federal-line"
        >
          {/* 1330 FEDERAL AVE <br /> LOS ANGELES, CA 90025 */}
          {ADDRESS_COMPANY} <br /> {CITY_COMPANY}, {STATE_COMPANY} {ZIP_CODE_COMPANY}
        </a>
      </div>

      {/* <br /> */}
      <div className="d-flex align-items-start mb-3">
        <img src={call} alt="call" className="me-2" />

        <a href={`tel:${PHONE_COMPANY}`} target="_blank" rel="noopener noreferrer">
          {formatPhoneText(PHONE_COMPANY)}
          <span className="sr-only">(opens in a new tab)</span>
        </a>
      </div>
    </>
  );
}
