import React from 'react';

import '../../assets/styles/components/admin/AdminFooter.scss';

const AdminFooter = () => <footer className="footer-admin" />;

export default AdminFooter;
