import React, { useState } from 'react';
import classNames from 'classnames';

import AdminHeader from './AdminHeader';
import AdminFooter from './AdminFooter';
import AdminMenu from './AdminMenu';

import '../../assets/styles/components/admin/AdminLayout.scss';

const AdminLayout = ({ children, location, isLogged }) => {
  const [compactMenu, setCompactMenu] = useState(false);
  const toggleCompactMenu = () => setCompactMenu(!compactMenu);
  const [openMenu, setOpenMenu] = useState(false);
  const toggleOpenMenu = () => setOpenMenu(!openMenu);
  return (
    <div className={classNames('admin-layout', { 'compact-menu': compactMenu, open: openMenu })}>
      <AdminMenu
        compactMenu={compactMenu}
        toggleCompactMenu={toggleCompactMenu}
        toggleOpenMenu={toggleOpenMenu}
      />

      <div className="admin-layout__right">
        <AdminHeader
          location={location}
          isLogged={isLogged}
          openMenu={openMenu}
          toggleOpenMenu={toggleOpenMenu}
        />
        <div className="container-fluid mb-4">{children}</div>

        <AdminFooter isLogged={isLogged} />
      </div>
    </div>
  );
};

export default AdminLayout;
