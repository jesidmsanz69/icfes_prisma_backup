import React from 'react';
import PropTypes from 'prop-types';
import { Alert } from 'react-bootstrap';

const propTypes = {
  text: PropTypes.string,
  type: PropTypes.number,
};
function MessageBox(props) {
  const { text } = props;
  let { type } = props;
  if (type > 3 || type < 0) {
    type = 1;
  }
  const messageType = [
    'info',
    'success',
    'warning',
    'danger',
    'primary',
    'secondary',
    'light',
    'dark',
  ];
  return <Alert variant={messageType[type]}>{text}</Alert>;
}

MessageBox.propTypes = propTypes;

export default MessageBox;
