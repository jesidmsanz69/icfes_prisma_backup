import React from 'react';

import '../assets/styles/components/Footer.scss';
import { Link } from 'react-router-dom';
import logo from '../assets/static/images/logo-small.png';
import equalHousingOpportunity from '../assets/static/images/icons/equal-housing-opportunity.svg';
import {
  formatPhoneText,
  // NAME_COMPANY,
  PHONE_COMPANY,
  ADDRESS_COMPANY,
  CITY_COMPANY,
  STATE_COMPANY,
  ZIP_CODE_COMPANY,
} from '../../server/utils/utilities';

const Footer = () => (
  <footer>
    <div className="container d-flex flex-column flex-md-row p-2 footercontact justify-content-between mb-4">
      <div>
        <img src={logo} alt="Logo" width="90" aria-label="Logo image" />
      </div>
      <div className="addressAndNumber">
        <a
          href="http://maps.google.com?q=1330+Federal+Ave++Los+Angeles+CA+90025"
          target="_blank"
          rel="noreferrer"
        >
          {ADDRESS_COMPANY} {CITY_COMPANY}, {STATE_COMPANY} {ZIP_CODE_COMPANY}
          <span className="sr-only">(opens in a new tab)</span>
        </a>
        <br />
        <a href={`tel:${PHONE_COMPANY}`} target="_blank" rel="noopener noreferrer">
          {formatPhoneText(PHONE_COMPANY)}
          <span className="sr-only"> (opens in a new tab)</span>
        </a>
        <br />
      </div>
      <Link to="/web-accessibility" className="accessibilityfooter">
        Web Accessibility
      </Link>
      <img src={equalHousingOpportunity} width="60" height="60" alt="equal Housing Opportunity" className="logoequal" />
      <div className="sitemap mb-0">
        <Link to="/sitemap">
          Sitemap HTML
        </Link>
        <br />
        <a href="/sitemap.xml" target="_blank" rel="noopener noreferrer">
          Sitemap XML
        </a>
      </div>
    </div>
    <p id="byit49">
      <a
        href="https://it49.com/single-property-website"
        target="_blank"
        rel="noopener noreferrer"
        className="ms-3"
      >
        Single Property Web Design by IT49 Multimedia
      </a></p>
  </footer>
);

export default Footer;
