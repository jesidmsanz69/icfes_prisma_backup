import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link, NavLink } from 'react-router-dom';
// import classNames from 'classnames';
import { Navbar, Nav, Container } from 'react-bootstrap';

// import gravatar from '../utils/gravatar';
import { logoutRequest } from '../redux/actions/loginActions';
// import { userIsInRole } from '../utils/auth';
// import logo from '../assets/static/logo.jpg';
// import callHeaderIcon from '../assets/static/icon/call-header.svg';
import logo from '../assets/static/images/logo-small.png';
import '../assets/styles/components/Header.scss';
import ImageWebp from './custom/ImageWebp';
// import utilities from '../utils/utilities';

class Header extends Component {
  // constructor(props) {
  //   super(props);
  //   this.hideNav = props.hideNav;
  // }

  render() {
    // console.log('this.hideNav :>> ', this.props);
    return (
      <>
        <a href="#content" className="visually-hidden visible-when-focused bypass-block-link">
          Skip Navigation
        </a>
        <Navbar collapseOnSelect expand="lg" className={!this.props.hideNav ? 'in' : ''}>
          <Container className="align-items-center">
            <Navbar.Brand as={Link} to="/">
              <ImageWebp src={logo} alt="Logo Live abstract" width="90" />
            </Navbar.Brand>
            <Navbar.Toggle aria-controls="responsive-navbar-nav" />
            <Navbar.Collapse id="responsive-navbar-nav">
              <Nav className="ms-auto ">
                <NavLink to="/floorplans">Availability</NavLink>
                <NavLink to="/photogallery">Gallery</NavLink>
                <NavLink to="/amenities">Amenities</NavLink>
                <NavLink to="/BrentwoodNeighborhood">Neighborhood</NavLink>
                {/* <NavLink to="#About">About</NavLink> */}
                <NavLink to="/contactUs">Contact Us</NavLink>
              </Nav>
              {/* <Nav>
                <Nav.Link href="#deets">More deets</Nav.Link>
                <Nav.Link eventKey={2} href="#memes">
                  Dank memes
                </Nav.Link>
              </Nav> */}
            </Navbar.Collapse>
          </Container>
        </Navbar>
      </>
    );
  }
}

const mapStateToProps = ({ login }) => ({ login });

const mapDispatchToProps = {
  logoutRequest,
};

export default connect(mapStateToProps, mapDispatchToProps)(Header);
