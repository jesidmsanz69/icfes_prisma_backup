import React from 'react';

import wantToBe from '../assets/static/images/background/wantToBe.jpg';
import wantToBeMobile from '../assets/static/images/background/mobile/wantToBe-mobile.jpg';
import fruit from '../assets/static/images/background/second-section-map-direction.jpg';
import fruitMobile from '../assets/static/images/background/mobile/second-section-map-direction-mobile.jpg';
import '../assets/styles/components/FavoriteSpots.scss';
import ImageWebp from './custom/ImageWebp';

export default function FavoriteSpots({ home }) {
  return (
    <>
      <section
        id="want-to-be"
        className="d-flex flex-column flex-md-row justify-content-center flex-wrap align-items-stretch"
      >
        <div className={`content-left ${!home && 'p-5'}`}>
          <div className="contet__left">
            <h2 className="h2">
              Favorite Spots
              <br /> We Recommend
            </h2>
            {/* <ul> */}
            <p>
              There are a myriad of fantastic restaurants just walking distance on San Vicente
              Boulevard, including: Baltaire, Pizzana, Katsuya and Kreation to name a few. In
              addition, there are some of the best coffee shops around: Alfred’s, Caffe Luxxe and
              Coral Tree. For all your shopping needs we are conveniently located minutes away from
              Whole Foods, Vicente Foods, Trader Joe’s and Ralphs. The building is also adjacent to
              Sawtelle Street and Japantown with premiere sushi restaurants, ramen bars and shaved
              ice shops.
            </p>
            <p>
              If you’re looking for a one stop shop for dining, shopping and events, there’s
              Brentwood Gardens Plaza and the Brentwood Country Mart.
            </p>
            <p>
              For hiking, mountain biking and other outdoor activities, you’re just miles from some
              of the best trails in the Santa Monica mountains, Los Liones, Westridge and Temescal.
              Or you can take a quick drive to the beach for a surf session or watch the California
              sunset at the Santa Monica or Malibu Pier.
            </p>
            <p>
              Brentwood Farmers Market is every Sunday from 9am-2pm with fresh produce and prepared
              food items weekly from local purveyors.
            </p>
            {/* </ul> */}
            {home && (
              <a className="btn btn-primary btn-color  mt-5 mb-5" href="/BrentwoodNeighborhood">
                EXPLORE NEIGHBORHOOD
              </a>
            )}
          </div>
        </div>
        <div className="content-right">
          {home ? (
            <ImageWebp
              // width="100%"
              // height="100%"
              src={wantToBe}
              mobilesrc={wantToBeMobile}
              alt="Full Suit"
              className="img-fluid"
            />
          ) : (
            <ImageWebp
              // width="100%"
              // height="100%"
              src={fruit}
              mobilesrc={fruitMobile}
              alt="Full Suit"
              className="img-fluid"
            />
          )}
        </div>
      </section>
    </>
  );
}
