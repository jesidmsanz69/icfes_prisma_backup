import axios from 'axios';

const textConfirmation = `This is a confirmation email that we receive your information.`;

export function createBodyEmail(name, email, phone, message, sendInformation, subject) {
  return `
    Full Name: ${name}<br/>
    Email: ${email}<br/>
    Phone: ${phone}<br/>
    Message: ${message}<br/>
    sendInformation: ${sendInformation ? 'Yes' : 'No'}<br/>
  `;
}

export async function sendEmail(fromName, to, replyTo, subject, body, confirmationEmail = false) {
  const bodyEmail = `${body} ${confirmationEmail ? textConfirmation : ''}`;
  await axios
    .post('https://api.it49.com/api/emails', {
      fromName,
      to,
      replyTo,
      subject,
      body: bodyEmail,
      code: '',
      token: 'vFKXw4Wgaj7x91dmCYLhNryDzisHeOfB',
      cc: '',
      bcc: '',
      local: true,
    })
    .then(({ data }) => {
      console.log('data', data);
    });
}
