const importAll = (route) => {
  const images = {};
  // eslint-disable-next-line array-callback-return
  route.keys().map((item, index) => {
    images[item.replace('./', '')] = route(item);
  });
  return images;
};

const importImages = (imagesDirectory) => {
  return importAll(require.context(imagesDirectory, false, /\.(png|jpe?g|svg)$/));
};

export default {
  importAll,
  importImages,
};
