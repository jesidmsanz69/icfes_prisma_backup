function getExtensionFile(fileName) {
  return fileName.split('.').pop();
}
module.exports = { getExtensionFile };
