/* eslint-disable no-restricted-globals */
const qs = require('qs');
const utilities = require('../../server/utils/utilities');
const materialTable = require('./materialTableUtils');

function questionDelete() {
  return confirm("Are you sure you want to delete this record?\nThis process can't be reversed.");
}

function getQuery(location) {
  return qs.parse(location.search, { ignoreQueryPrefix: true });
}
module.exports = { ...utilities, questionDelete, getQuery, materialTable };
