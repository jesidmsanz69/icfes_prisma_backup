import React, { Component } from 'react';
// import DataTable from 'react-data-table-component';
import axios from 'axios';
import { Card } from 'react-bootstrap';
import MaterialTable from 'material-table';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEye, faTrash } from '@fortawesome/free-solid-svg-icons';
import MessageBox from '../../../components/MessageBox';
import utilities from '../../../utils/utilities';
import Loading from '../../../components/custom/Loading';

export default class ContactsList extends Component {
  constructor() {
    super();
    this.state = {
      data: [],
      error: false,
      loading: false,
    };
    this.loadData = this.loadData.bind(this);
    this.deleteContact = this.deleteContact.bind(this);
  }

  loadData() {
    console.log('loadData');
    axios
      .get('contacts')
      .then(({ data }) => {
        // console.log('data', data);
        if (data.body && data.body.length) this.setState({ data: data.body, loading: false });
        else {
          console.log('no data');
          this.setState({ error: true, loading: false, data: [] });
        }
      })
      .catch((err) => {
        console.log('err', err);
        this.setState({ error: true, loading: false, data: [] });
      });
  }

  deleteContact(id) {
    if (utilities.questionDelete()) {
      {
        axios
          .delete(`contacts/${id}`)
          .then(({ data }) => {
            // console.log('data', data);
            if (data.body && data.body === 1) {
              alert('Deleted successfully');
              const { data } = this.state;
              const finalData = (data || []).filter((item) => item.id !== id);
              this.setState({ data: finalData });
            } else {
              console.log('no data');
              this.setState({ error: true, loading: false });
            }
          })
          .catch((err) => {
            console.log('err', err);
            this.setState({ error: true, loading: false });
          });
      }
    }
  }

  componentDidMount() {
    this.loadData();
  }

  render() {
    if (this.state.loading) {
      return <Loading />;
    }
    if (this.state.error) {
      {
        this.state.error && <MessageBox type={3} text="Has occurred an error, please try again!" />;
      }
    }

    return (
      <Card>
        <Card.Header className="d-flex justify-content-between align-items-center">
          <h1 className="card-title">Contacts</h1>
        </Card.Header>
        <Card.Body>
          <MaterialTable
            columns={[
              { title: 'First Name', field: 'firstName', headerStyle: { fontWeight: 'bold' } },
              { title: 'Last Name', field: 'lastName', headerStyle: { fontWeight: 'bold' } },
              { title: 'Email', field: 'email', headerStyle: { fontWeight: 'bold' } },
              { title: 'Phone', field: 'phone', headerStyle: { fontWeight: 'bold' } },
              {
                title: 'Date',
                field: 'createdAt',
                headerStyle: { fontWeight: 'bold' },
                render: ({ createdAt }) => utilities.formatDateTime(createdAt),
              },
            ]}
            localization={utilities.materialTable.localization}
            data={this.state.data || []}
            title=" "
            actions={[
              {
                icon: () => <FontAwesomeIcon icon={faEye} />,
                tooltip: 'View',
                onClick: (event, rowData) => {
                  this.props.history.push(`/admin/contacts/${rowData.id}`);
                },
              },
              {
                icon: () => <FontAwesomeIcon icon={faTrash} />,
                tooltip: 'Delete',
                onClick: (event, rowData) => {
                  console.log('rowData', rowData);
                  this.deleteContact(rowData.id);
                },
              },
            ]}
            options={utilities.materialTable.options}
          />
        </Card.Body>
      </Card>
    );
  }
}
