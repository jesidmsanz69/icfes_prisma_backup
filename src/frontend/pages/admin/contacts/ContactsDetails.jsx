import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';
import MessageBox from '../../../components/MessageBox';

export default class ContactsDetails extends Component {
  constructor() {
    super();
    this.state = {
      data: {},
      error: false,
      loading: false,
    };
    this.loadData = this.loadData.bind(this);
  }

  loadData() {
    const { id } = this.props.match.params;
    axios
      .get(`contacts/${id}`)
      .then(({ data }) => {
        // console.log('data', data);
        if (data.body && data.body.id) this.setState({ data: data.body, loading: false });
        else {
          console.log('no data');
          this.setState({ error: true, loading: false });
        }
      })
      .catch((err) => {
        console.log('err', err);
        this.setState({ error: true, loading: false });
      });
  }

  componentDidMount() {
    this.loadData();
  }

  render() {
    if (this.state.loading) {
      return (
        <div className="contentloading">
          <img src={loadingimg} alt="Loading Content..." />
        </div>
      );
    }
    if (this.state.error) {
      {
        this.state.error && <MessageBox type={3} text="Has occurred an error, please try again!" />;
      }
    }
    const { data } = this.state;
    return (
      <div className="container container-detail">
        <h1>Contact Details</h1>
        <div className="mb-3">
          <strong className="w-100">First Name:</strong>
          <span>{data.firstName}</span>
        </div>
        <div className="mb-3">
          <strong className="w-100">Last Name:</strong>
          <span>{data.lastName}</span>
        </div>
        <div className="mb-3">
          <strong className="w-100">Phone:</strong>
          <span>{data.phone}</span>
        </div>
        <div className="mb-3">
          <strong className="w-100">Email:</strong>
          <span>{data.email}</span>
        </div>
        <div className="mb-3">
          <strong className="w-100">Message:</strong>
          <span>{data.message}</span>
        </div>
        <p>
          <Link className="btn btn-secondary" to="/admin/contacts">
            Back
          </Link>
        </p>
      </div>
    );
  }
}
