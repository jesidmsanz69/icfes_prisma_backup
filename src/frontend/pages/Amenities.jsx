import React, { Component } from 'react';

import { Col, Container, Row } from 'react-bootstrap';
import { Helmet } from 'react-helmet';
import Header from '../components/Header';
import '../assets/styles/components/Amenities.scss';
import Footer from '../components/Footer';
import ListAmenities from '../components/ListAmenities';
import ScrollToTop from '../components/scroll-to-top';

export default class Amenities extends Component {
  render() {
    return (
      <>
        <Helmet>
          <title>Amenities at Abstract 1330, Luxury Apartments in Brentwood</title>
          <meta
            name="description"
            content="Luxury Apartment Amenities in Brentwood: Roof deck, Vehicle charging stations,  Controlled Access Entry, Subterranean Assigned Parking, Walk-In Custom Closets."
          />
        </Helmet>
        <ScrollToTop />
        <Header />
        <div id="content" className="bypass-block-target " tabIndex="-1">
          <div className="image-background-amenities">
            <p className="sr-only">images amenities</p>
          </div>
          <div className="description-photo">
            <span className="photo-site-beach">Photograph: Courtesy Unsplash/Gerson Repreza</span>
          </div>
          <ListAmenities />
        </div>
        <Footer />
      </>
    );
  }
}
