import React, { Component } from 'react';

import { Card, Col, Container, Row } from 'react-bootstrap';
import { Helmet } from 'react-helmet';
import { Link } from 'react-router-dom';
import Header from '../components/Header';
import '../assets/styles/components/ContactUs.scss';
import Footer from '../components/Footer';
import FormContact from '../components/FormContact';
import location from '../assets/static/images/icons/location.svg';
import call from '../assets/static/images/icons/call.svg';
import logoColorSmall from '../assets/static/images/logoColorSmall.png';
import { PHONE_COMPANY, formatPhoneText } from '../../server/utils/utilities';
import ScrollToTop from '../components/scroll-to-top';

export default class ContactUs extends Component {
  render() {
    return (
      <>
        <Helmet>
          <title>Contact Us Abstract 1330, 1330 Federal Ave, West Los Angeles</title>
          <meta
            name="description"
            content={`Contact Us: Abstract 1330, 1330 FEDERAL AVE LOS ANGELES, CA 90025 call us: ${formatPhoneText(
              PHONE_COMPANY
            )}`}
          />
        </Helmet>
        <ScrollToTop />
        <Header />
        <div id="content" className="bypass-block-target " tabIndex="-1">
          <Container className="content-contactUs content-section-p">
            <h3 className="h2 text-center">Contact Us</h3>
            <Row className="justify-content-center">
              <Col lg={6}>
                <Card className="pl-5 pr-5 mb-5 border-0">
                  <Card.Body>
                    {/* <Card.Text> */}
                    <div className="contentform">
                      <FormContact
                        bed={this.props.match.params.bed}
                        bath={this.props.match.params.bath}
                      />
                    </div>
                    {/* </Card.Text> */}
                  </Card.Body>
                </Card>
              </Col>
              <Col lg={3} className="ps-3">
                <img src={logoColorSmall} alt="logo abstract" width="150" className="mb-4" />

                <div className="d-flex align-items-start mb-3">
                  <img src={location} alt="Location" className="me-2" />
                  <a
                    target="_blank"
                    href="http://maps.google.com?q=1330+Federal+Ave++Los+Angeles+CA+90025"
                    rel="noreferrer"
                  >
                    1330 FEDERAL AVE <br /> LOS ANGELES, CA 90025
                  </a>
                </div>

                {/* <br /> */}
                <div className="d-flex align-items-start mb-3">
                  <img src={call} alt="call" className="me-2" />

                  <a href={`tel:${PHONE_COMPANY}`} target="_blank" rel="noopener noreferrer">
                    <span className="click_to_call">{formatPhoneText(PHONE_COMPANY)}</span>
                  </a>
                </div>
                <p>
                  Contact Hours: <br />
                  Monday – Friday 10 AM TO 5 PM <br />
                  Saturday &amp; Sunday 10 AM TO 3 PM
                </p>
                {/* <Row className="text-center rowButtonContact">
                  <Col sm={6} className="mb-3">
                    <Link to="/floorplans" className="btn btn-primary btn-block">
                      Lease Now
                    </Link>
                  </Col>
                  <Col sm={6} className="mb-3">
                    <a href="#/" target="_blank" className="btn btn-primary btn-block">
                      Pay Rent
                    </a>
                  </Col>
                  <Col sm={12}>
                    <a href="#/" target="_blank" className="btn btn-primary btn-block">
                      Maintenance Request
                    </a>
                  </Col>
                </Row> */}
              </Col>
            </Row>
          </Container>
        </div>

        <Footer bottom={1} />
      </>
    );
  }
}
