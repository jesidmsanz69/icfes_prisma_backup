import React, { Component } from 'react';

import { Container } from 'react-bootstrap';
import { Helmet } from 'react-helmet';

import Header from '../components/Header';
import '../assets/styles/components/PhotoGallery.scss';
import Footer from '../components/Footer';
import SlideWithThumbs from '../components/custom/SlideWithThumbs';
import ScrollToTop from '../components/scroll-to-top';

function importAll(r) {
  const images = {};
  r.keys().forEach((item, index) => {
    const module = r(item);
    images[item.replace('./', '')] = module.__esModule ? module.default : module;
  });
  return images;
}

let imagesgallery;
let imagesgallerylarge;

if (typeof window !== 'undefined') {
  imagesgallery = importAll(
    require.context('../assets/static/images/gallerySlide', false, /\.(png|jpe?g|svg)$/)
  );
  imagesgallerylarge = importAll(
    require.context('../assets/static/images/gallerySlide/large', false, /\.(png|jpe?g|svg)$/)
  );
} else {
  const requireContext = require('require-context');
  imagesgallery = importAll(
    requireContext(
      '../../src/frontend/assets/static/images/gallerySlide',
      false,
      /\.(png|jpe?g|svg)$/
    )
  );
  imagesgallerylarge = importAll(
    requireContext(
      '../../src/frontend/assets/static/images/gallerySlide/large',
      false,
      /\.(png|jpe?g|svg)$/
    )
  );
}

export default class PhotoGallery extends Component {
  render() {
    return (
      <>
        <Helmet>
          <title>Photo Gallery, 1330 Federal Ave Los Angeles, CA 90025</title>
          <meta
            name="description"
            content="Photo Gallery, 1330 Federal Ave  Los Angeles, CA 90025"
          />
        </Helmet>
        <ScrollToTop />
        <Header />
        <div id="content" className="bypass-block-target " tabIndex="-1">
          <Container className="content-photo-gallery content-section-p">
            <h3 className="h2 text-center">Photo Gallery</h3>
            <SlideWithThumbs listnormal={imagesgallery} listlarge={imagesgallerylarge} />
            <h3 className="h2 text-center">Virtual Tour</h3>

            <div className="embed-responsive embed-responsive-16by9 mb-3">
              <iframe
                className="embed-responsive-item"
                src="https://my.matterport.com/show/?m=S7JaUmgLteq"
                id="iframevideoproperty"
                title="Virtual Tour"
                allowFullScreen
              ></iframe>
            </div>
          </Container>
        </div>
        <Footer />
        <div id="blueimp-gallery" className="blueimp-gallery blueimp-gallery-controls">
          <div className="slides" />
          <h3 className="title" />
          <a role="button" className="prev">
            ‹
          </a>
          <a role="button" className="next">
            ›
          </a>
          <a role="button" className="close">
            ×
          </a>
        </div>
      </>
    );
  }
}
