import React, { Component } from 'react';
import { FormText as Input, Button, FormLabel as Label } from 'react-bootstrap';
import { connect } from 'react-redux';

import Axios from 'axios';
import { resetPasswordRequest } from '../../redux/actions/userActions';
import { message } from '../../utils/notification';

class AdminResetPassword extends Component {
  constructor(props) {
    super(props);
    this.state = {
      form: {
        userId: '',
        newPassword: '',
      },
      users: null,
      errors: '', //Client errors
    };

    this.clearForm = this.clearForm.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.validateSubmit = this.validateSubmit.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.loadUsers = this.loadUsers.bind(this);
  }

  clearForm() {
    this.setState({
      form: {
        userId: '',
        newPassword: '',
      },
    });
  }

  handleChange(event) {
    const { form } = this.state;
    this.setState({
      form: {
        ...form,
        [event.target.name]: event.target.value,
      },
    });
  }

  validateSubmit(target) {
    const errors = {};
    const { form } = this.state;
    const requiredMessage = 'Campo requerido';
    if (target) {
      form[target.name] = target.value;
      if (target.required && target.value === '') {
        errors[target.name] = requiredMessage;
      }
    } else {
      if (form.userId === '') errors.userId = requiredMessage;
      if (form.newPassword === '') errors.newPassword = requiredMessage;
    }

    this.setState({ errors });
    const isValid = Object.keys(errors).length === 0;
    return isValid;
  }

  async handleSubmit(event) {
    event.preventDefault();
    try {
      const isValid = this.validateSubmit();
      if (isValid) {
        const { form } = this.state;
        this.props.resetPasswordRequest(form);
      }
    } catch (error) {
      message('Error al validar los datos', 3);
    }
  }

  async loadUsers() {
    try {
      const data = await Axios.get('users');
      const users = data.data.body;
      this.setState({ users });
    } catch (error) {
      console.log('[ERROR loadUsers]: ', error);
    }
  }

  loadInitialData() {
    this.loadUsers();
  }

  componentDidMount() {
    this.loadInitialData();
  }

  componentDidUpdate(prevProps) {
    if (prevProps.user.hasError !== this.props.user.hasError && this.props.user.hasError) {
      const msg =
        typeof this.props.user.error === 'string'
          ? this.props.user.error
          : 'Ha ocurrido un error al cambiar la contraseña';
      message(msg, 3);
    }

    if (prevProps.user.data !== this.props.user.data && this.props.user.data) {
      if (typeof this.props.user.data === 'string') {
        message(this.props.user.data, 3);
      } else {
        message('Contraseña cambiada con éxito');
        this.clearForm();
      }
    }
  }

  render() {
    const { users, form, errors } = this.state;
    const { loading } = this.props.user;
    return (
      <div className="container">
        <h1>Restablecer contraseña</h1>
        <form onSubmit={this.handleSubmit}>
          <div className="row">
            <div className="col-md-4">
              <Label htmlFor="txtuserId">Usuario</Label>
              <Input
                type="select"
                id="txtuserId"
                name="userId"
                value={form.userId}
                onChange={this.handleChange}
                invalid={!!errors.userId}
              >
                <option key="0" value="">
                  Selecione...
                </option>
                {users != null &&
                  users.map((item) => (
                    <option key={item.id} value={item.id}>
                      {item.firstName} {item.lastName}
                    </option>
                  ))}
              </Input>
            </div>
            <div className="col-md-4">
              <Label htmlFor="txtNewPassword">Nueva contraseña</Label>
              <Input
                type="password"
                id="txtNewPassword"
                name="newPassword"
                value={form.newPassword}
                onChange={this.handleChange}
                invalid={!!errors.newPassword}
                placeholder="Nueva contraseña"
              />
            </div>
            <div className="col-md-4 d-flex align-items-end">
              <Button type="submit" color="primary" disabled={loading}>
                {loading ? 'Cargando...' : 'Cambiar contraseña'}
              </Button>
            </div>
          </div>
        </form>
      </div>
    );
  }
}

const mapDispatchToProps = ({ login, user }) => ({ login, user });

export default connect(mapDispatchToProps, { resetPasswordRequest })(AdminResetPassword);
