import React, { Component } from 'react';
import { FormControl as Input, Button, FormLabel as Label } from 'react-bootstrap';
import { connect } from 'react-redux';

import '../../assets/styles/components/admin/ChangePassword.scss';
import { changePasswordRequest } from '../../redux/actions/userActions';
import { message } from '../../utils/notification';

class ChangePassword extends Component {
  constructor(props) {
    super(props);
    this.state = {
      form: {
        currentPassword: '',
        newPassword: '',
      },
      // errors: '', //Client errors
    };

    this.isProtected = true;

    this.clearForm = this.clearForm.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.validateSubmit = this.validateSubmit.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  clearForm() {
    this.setState({
      form: {
        currentPassword: '',
        newPassword: '',
      },
    });
  }

  handleChange(event) {
    const { form } = this.state;
    this.setState({
      form: {
        ...form,
        [event.target.name]: event.target.value,
      },
    });
  }

  validateSubmit(target) {
    const errors = {};
    const { form } = this.state;
    const requiredMessage = 'Campo requerido';
    if (target) {
      form[target.name] = target.value;
      if (target.required && target.value === '') {
        errors[target.name] = requiredMessage;
      }
    } else {
      if (form.currentPassword === '') errors.currentPassword = requiredMessage;
      if (form.newPassword === '') errors.newPassword = requiredMessage;
    }

    // this.setState({ errors });
    const isValid = Object.keys(errors).length === 0;
    return isValid;
  }

  async handleSubmit(event) {
    event.preventDefault();
    try {
      const isValid = this.validateSubmit();
      if (isValid) {
        const { form } = this.state;
        this.props.changePasswordRequest(form);
      }
    } catch (error) {
      message('Error al validar los datos', 3);
    }
  }

  componentDidUpdate(prevProps) {
    if (prevProps.user.hasError !== this.props.user.hasError && this.props.user.hasError) {
      const msg =
        typeof this.props.user.error === 'string'
          ? this.props.user.error
          : 'Ha ocurrido un error al cambiar la contraseña';
      message(msg, 3);
    }

    if (prevProps.user.data !== this.props.user.data && this.props.user.data) {
      if (typeof this.props.user.data === 'string') {
        message(this.props.user.data, 3);
      } else {
        message('Contraseña cambiada con éxito');
        this.clearForm();
      }
    }
  }

  render() {
    const { loading } = this.props.user;
    return (
      <div className="container">
        <h1>Cambiar contraseña</h1>
        <form onSubmit={this.handleSubmit}>
          <div className="row">
            <div className="col-md-4">
              <Label htmlFor="txtCurrentPassword">Contraseña actual</Label>
              <Input
                type="password"
                id="txtCurrentPassword"
                name="currentPassword"
                value={this.state.form.currentPassword}
                onChange={this.handleChange}
                placeholder="Contraseña anterior"
              />
            </div>
            <div className="col-md-4">
              <Label htmlFor="txtNewPassword">Nueva contraseña</Label>
              <Input
                type="password"
                id="txtNewPassword"
                name="newPassword"
                value={this.state.form.newPassword}
                onChange={this.handleChange}
                placeholder="Nueva contraseña"
              />
            </div>
            <div className="col-md-4 d-flex align-items-end">
              <Button type="submit" color="primary" disabled={loading}>
                {loading ? 'Cargando...' : 'Cambiar contraseña'}
              </Button>
            </div>
          </div>
        </form>
      </div>
    );
  }
}

const mapDispatchToProps = ({ login, user }) => ({ login, user });

export default connect(mapDispatchToProps, { changePasswordRequest })(ChangePassword);
