import React from 'react';
import { Helmet } from 'react-helmet';
import Container from 'react-bootstrap/Container';
import Breadcrumb from 'react-bootstrap/Breadcrumb';
import Header from '../components/Header';
import Footer from '../components/Footer';
import ScrollToTop from '../components/scroll-to-top';
import ImageWebp from '../components/custom/ImageWebp';
import {
  formatPhoneText,
  NAME_COMPANY,
  PHONE_COMPANY,
  ADDRESS_COMPANY,
  CITY_COMPANY,
  STATE_COMPANY,
  ZIP_CODE_COMPANY,
} from '../utils/utilities';

class WebAccessibility extends React.Component {
  render() {
    return (
      <>
        <Helmet>
          <title>Web Accessibility | Abstract 1330</title>
          <meta
            name="description"
            content="Read the web accessibility statement for Abstract 1330. Our commitment and approach to maintaining an accessible website."
          />
        </Helmet>
        <ScrollToTop />
        <Header mode="inner" />

        <div id="content" className="bypass-block-target" tabIndex="-1">
          <Container className="contentinner content-section-p">
            <h1 className="headingbarbyside">Web Accessibility Statement</h1>
            <br />
            <h2 className="text-left">
              Our commitment and approach to maintaining an accessible website
            </h2>
            <p>{NAME_COMPANY} is 100% committed to:</p>
            <ul>
              <li>
                Maintaining a diverse, inclusive and accessible website. {NAME_COMPANY} is
                constantly improving and solving usability issues in order to deliver new solutions
                to further improve the accessibility of our site.
              </li>
              <li>
                Ensuring that this website achieves, ensures, and complies with the best practices
                and standards defined by Section 508 of the U.S. Rehabilitation Act and the Web
                Content Accessibility Guidelines of the World Wide Web Consortium.
              </li>
              <li>
                Making sure that all new information on the website will achieve “Level AA”
                conformance to the Web Content Accessibility Guidelines (WCAG) 2.0.
              </li>
              <li>
                Including accessibility functions when we procure 3rd-party systems/apps or upgrades
                to our existing systems.
              </li>
            </ul>
            <br />
            <h2>Our Current Accessibility Features include:</h2>
            <ol>
              <li>
                Title tags and meta descriptions to provide information on what the page is about.
              </li>
              <li>Alternate text – an attribute added to an image to describe it.</li>
              <li>Style sheets and JavaScript to enhance functionality and appearance.</li>
              <li>Structural markups that allow the correct visualization of all content.</li>
              <li>The interrelation between forms and labels.</li>
              <li>Interconnection of data cells within data tables, along with their headers.</li>
            </ol>

            <p>
              More accessibility efforts are currently on the way. Every time we improve our
              website, we will update the changes here within our accessibility statement. This way,
              you will be able to learn about the advancements we’re making.
            </p>
            <br />
            <h2>How to send feedback regarding this website’s accessibility</h2>
            <p>We welcome feedback on the accessibility of {NAME_COMPANY}’s website.</p>
            <ul>
              <li>Phone us on {formatPhoneText(PHONE_COMPANY)}</li>
              <li>
                Visit us at {ADDRESS_COMPANY} {CITY_COMPANY}, {STATE_COMPANY} {ZIP_CODE_COMPANY}
              </li>
            </ul>
          </Container>
        </div>
        <Footer />
      </>
    );
  }
}

export default WebAccessibility;
