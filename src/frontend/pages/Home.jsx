import React, { Component } from 'react';

import { Helmet } from 'react-helmet';
import { Container, Card } from 'react-bootstrap';
import '../assets/styles/components/Home.scss';
import logo from '../assets/static/images/logo.png';
// import aboutHelio from '../assets/static/images/background/about-helio.jpg';
import fullSuit from '../assets/static/images/background/fullSuit.jpg';
import fullSuitMobile from '../assets/static/images/background/mobile/fullSuit-mobile.jpg';
import wantToBe from '../assets/static/images/background/wantToBe.jpg';
import location from '../assets/static/images/icons/location.svg';
import call from '../assets/static/images/icons/call.svg';
import Header from '../components/Header';
import FormContact from '../components/FormContact';
// import { PHONE_COMPANY, formatPhoneText } from '../utils/utilities';
import Footer from '../components/Footer';
import ListAmenities from '../components/ListAmenities';
import {
  PHONE_COMPANY,
  ADDRESS_COMPANY,
  CITY_COMPANY,
  STATE_COMPANY,
  ZIP_CODE_COMPANY,
  formatPhoneText,
} from '../../server/utils/utilities';
import PhoneAddress from '../components/PhoneAddress';
import FavoriteSpots from '../components/FavoriteSpots';
import image2 from '../assets/static/images/background/image-2.jpg';
import mapa from '../assets/static/images/background/mapa.png';
import mapaMobile from '../assets/static/images/background/mobile/mapaMobile.png';
import image2Mobile from '../assets/static/images/background/mobile/image-2-mobile.jpg';
import ImageWebp from '../components/custom/ImageWebp';
import ScrollToTop from '../components/scroll-to-top';

// import { Link, Element, Events, scrollSpy, scroller } from 'react-scroll';

const position = [34.04792376863582, -118.45633978940867];

class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      hideNav: true,
      showMap: false,
      components: {},
    };
    this.handleScroll = this.handleScroll.bind(this);
    this.loadClientComponents = this.loadClientComponents.bind(this);
    this.handleMapaChange = this.handleMapaChange.bind(this);
  }

  handleMapaChange() {
    if (!this.state.showMap) {
      this.loadClientComponents();
    }
  }

  loadClientComponents() {
    const { Map, Marker, Popup, TileLayer } = require('react-leaflet');
    this.setState({ showMap: true, components: { Map, Marker, Popup, TileLayer } });
  }

  handleScroll() {
    if (
      window.scrollY >= document.querySelector('#above-content-photo').offsetTop &&
      this.state.hideNav
    ) {
      this.setState({ hideNav: false });
    } else if (
      window.scrollY <= document.querySelector('#above-content-photo').offsetTop &&
      !this.state.hideNav
    ) {
      this.setState({ hideNav: true });
    }
  }

  componentDidMount() {
    //   // window.addEventListener('scroll', this.handleScroll);
    //   this.loadClientComponents();
    document.getElementById('imgMapa').addEventListener('mouseover', this.handleMapaChange);
  }

  render() {
    // console.log('this.hideNav home :>> ', PHONE_COMPANY);
    const { Map, Marker, Popup, TileLayer } = this.state.components;
    return (
      <>
        <Helmet>
          <title>Abstract 1330 Brand New Luxury Apartments in Brentwood</title>
          <meta
            name="description"
            content="THE ABSTRACT a brand new apartment community in the heart of West Los Angeles in Brentwood. Includes a rooftop deck, parking, and washer and dryers in units."
          />
        </Helmet>
        <ScrollToTop />
        <Header />

        <div id="content" className="bypass-block-target" tabIndex="-1">
          <div className="background-images-1">
            <ImageWebp
              src={image2}
              mobilesrc={image2Mobile}
              alt="Abstract"
              className="background-images__img"
            />
            <div className="top-section">
              <ImageWebp src={logo} width="250" alt="Abstract logo" />
            </div>
          </div>
          {/* Section2 */}
          <div className="content-nav-section">
            <p className="address">
              <a
                target="_blank"
                href="http://maps.google.com?q=1330+Federal+Ave++Los+Angeles+CA+90025"
                rel="noreferrer"
              >
                {/* 1330 Federal Ave&nbsp; Los Angeles, CA <span> 90025</span> */}
                {ADDRESS_COMPANY} {CITY_COMPANY}, {STATE_COMPANY} {ZIP_CODE_COMPANY}
              </a>
              <br />
              <a href={`tel:${PHONE_COMPANY}`} target="_blank" rel="noopener noreferrer">
                {formatPhoneText(PHONE_COMPANY)}
                <span className="sr-only"> (opens in a new tab)</span>
              </a>
            </p>
          </div>
          {/* Section3 */}
          <section id="above-content-photo" aria-label="image of property building">
            <p className="visually-hidden">image of property building</p>
          </section>
          {/* Section4 */}
          <section id="content-section" className="pb-5 pt-5">
            <Container>
              <h1 className="h2">Abstract 1330</h1>
              <p className="mb-5">
                Located just a short walk from the Brentwood village you will love the unique
                design, extra large bedrooms and relaxing lifestyle offered to residents at The
                Abstract. THE ABSTRACT is a “brand new” apartment community perfect for those who
                want very spacious expansive at home living in the heart of the city. Conveniently
                located close to the freeway and with breathtaking ocean views, this apartment
                community is perfect for those who enjoy being in a central location and have an
                active lifestyle. Amenities include rooftop deck, single spot parking and washer and
                dryers in all units.
              </p>
              <a className="btn btn-primary btn-color text-center me-3 mt-3" href="/floorplans">
                AVAILABILITY
              </a>
              <a className="btn btn-primary btn-color text-center me-3 mt-3" href="/photogallery">
                GALLERY
              </a>
              <a className="btn btn-primary btn-color text-center mt-3" href="/contactUs">
                CONTACT US
              </a>
            </Container>
          </section>
          {/* Section5 */}
          <section
            id="residence-section"
            className="d-flex flex-column flex-md-row justify-content-center flex-wrap align-items-stretch"
          >
            <div className="content-left">
              <ImageWebp
                // width="100%"
                // height="100%"
                src={fullSuit}
                mobilesrc={fullSuitMobile}
                alt="Full Suit"
                className="img-fluid"
              />
            </div>
            <div className="right-content">
              <div className="content__right">
                <h2 className="h2">Attention to Detail</h2>
                <p>
                  These beautifully designed residences at THE ABSTRACT are some of the largest
                  units currently for rent on the West Side of Los Angeles. They are ideal for
                  families, singles or roommates looking for a spacious living with lots of natural
                  light. They are suited well for live/work environment and come with side by side
                  single spot parking. Brand new ground up construction with state of the art
                  stainless steel kitchen appliances, front loading washer and dryer in each unit,
                  and keyless entry. Bathrooms have quartz counters, large oversized soaking tubs
                  and ample storage. Large expansive balconies and courtyards are wonderful for
                  fresh California breezes and enjoying the outdoors. No detail was left out, this
                  is the place you want to live!! Call us today{' '}
                  <a href={`tel:${PHONE_COMPANY}`} target="_blank" rel="noopener noreferrer">
                    {formatPhoneText(PHONE_COMPANY)}
                    <span className="sr-only"> (opens in a new tab)</span>
                  </a>
                  .
                </p>
                <a className="btn btn-primary btn-color mt-5 mb-5" href="/floorplans">
                  VIEW AVAILABILITY
                </a>
              </div>
            </div>
          </section>
          {/* Section6 */}
          <section className="above-amenities-photo" aria-label="image of lunch">
            <p className="visually-hidden">image of lunch</p>
          </section>
          <div className="description-photo">
            <span className="photo-site-beach">Photograph: Courtesy Unsplash/Gerson Repreza</span>
          </div>
          {/* Section7 */}
          <ListAmenities />
          {/* Section8 */}
          <FavoriteSpots home={true} />
          <section
            id="map-contact"
            className="d-flex flex-column flex-md-row justify-content-center flex-wrap align-items-stretch"
          >
            <div className="mapcontact">
              {this.state.showMap ? (
                <Map center={position} zoom={16} scrollWheelZoom={false}>
                  <TileLayer
                    url="https://{s}.basemaps.cartocdn.com/rastertiles/voyager/{z}/{x}/{y}{r}.png"
                    attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors &copy; <a href="https://carto.com/attributions">CARTO</a>'
                  />
                  <Marker position={position}>
                    <Popup>
                      <span>{ADDRESS_COMPANY}</span>
                    </Popup>
                  </Marker>
                </Map>
              ) : (
                <ImageWebp
                  src={mapa}
                  alt="Mapa"
                  onClick={this.handleMapaChange}
                  className="img-fluid"
                  id="imgMapa"
                  // width="1115"
                  // height="496"
                  mobilesrc={mapaMobile}
                />
              )}
            </div>
            <div className="right-content d-flex">
              <div className="content-contact p-3">
                <h2 className="h2">Contact us</h2>
                <PhoneAddress />

                <Card className="mt-4 pl-5 pr-5 card-width-small border-0">
                  <Card.Body className="p-0">
                    {/* <Card.Text> */}
                    <div className="contentform">
                      <FormContact />
                    </div>
                    {/* </Card.Text> */}
                  </Card.Body>
                </Card>
              </div>
            </div>
          </section>
          {/* Section10 */}
          {/* <section
            id="about-Helio-section"
            className="d-flex flex-column justify-content-center align-items-center"
          >
            <div className="width-wrapper d-flex flex-column flex-lg-row justify-content-center align-items-center">
              <div
                id="about-Helio-left"
                className="left-content d-flex flex-column justify-content-center align-items-center p-5 flex-fill"
              >
                <div className="inner">
                  <h2 className="h2">Pride of Place</h2>
                  <p className="detail-info">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur vel ex sed
                    nulla malesuada rhoncus. Aenean ornare facilisis vestibulum. Nunc lacinia
                    hendrerit mauris, ac mollis eros malesuada lobortis. Aliquam nunc metus,
                    pharetra vel volutpat mattis, fringilla quis risus.
                  </p>
                  <a href="/" className="btn btn-primary btn-color  mt-5 mb-5">
                    VISIT
                  </a>
                </div>
              </div>
              <div className="right-image" id="about-Helio-right">
                <img className="main-image" src={aboutHelio} alt="about" />
                <div className="overlay" />
                
              </div>
            </div>
          </section> */}
          {/* <section className="sr-only">
            <table width={638} cellPadding={7} cellSpacing={0}>
              <tbody>
                <tr style={{ fontSize: '21pt' }}>
                  <td>UNIT</td>
                  <td>BEDS</td>
                  <td>BATHS</td>
                  <td>AREA</td>
                </tr>
                <tr bgcolor="#b5d6e3">
                  <td>101</td>
                  <td>3</td>
                  <td>2</td>
                  <td>1771 sqft.</td>
                </tr>
                <tr>
                  <td>102, 202, 302, 402, PH #2</td>
                  <td>3</td>
                  <td>2</td>
                  <td>1719 sqft.</td>
                </tr>
                <tr bgcolor="#b5d6e3">
                  <td>103, 203, 303, 403, PH #3</td>
                  <td>2</td>
                  <td>2</td>
                  <td>1381 sqft.</td>
                </tr>
                <tr>
                  <td>104, 204, 304, 404, PH #4</td>
                  <td>2</td>
                  <td>2</td>
                  <td>1408 sqft.</td>
                </tr>
                <tr bgcolor="#b5d6e3">
                  <td>106, 206, 306</td>
                  <td>2</td>
                  <td>2</td>
                  <td>1372 sqft.</td>
                </tr>
                <tr>
                  <td>107</td>
                  <td>2</td>
                  <td>2</td>
                  <td>1599 sqft.</td>
                </tr>
                <tr bgcolor="#b5d6e3">
                  <td>201, 301, 401, PH #1</td>
                  <td>3</td>
                  <td>2</td>
                  <td>1742 sqft.</td>
                </tr>
                <tr>
                  <td>205, 305</td>
                  <td>2</td>
                  <td>2</td>
                  <td>1141 sqft.</td>
                </tr>
                <tr bgcolor="#b5d6e3">
                  <td>207</td>
                  <td>3</td>
                  <td>3</td>
                  <td>1907 sqft.</td>
                </tr>
                <tr>
                  <td>307, 407</td>
                  <td>2</td>
                  <td>2</td>
                  <td>1257 sqft.</td>
                </tr>
                <tr bgcolor="#b5d6e3">
                  <td>308, 408, PH #7</td>
                  <td>2</td>
                  <td>2</td>
                  <td>1362 sqft.</td>
                </tr>
                <tr>
                  <td>405</td>
                  <td>2</td>
                  <td>2</td>
                  <td>1523 sqft.</td>
                </tr>
                <tr bgcolor="#b5d6e3">
                  <td>406</td>
                  <td>2</td>
                  <td>2</td>
                  <td>1247 sqft.</td>
                </tr>
                <tr>
                  <td>PH #5</td>
                  <td>2</td>
                  <td>2</td>
                  <td>1400 sqft.</td>
                </tr>
                <tr bgcolor="#b5d6e3">
                  <td>PH #6</td>
                  <td>2</td>
                  <td>2</td>
                  <td>1264 sqft.</td>
                </tr>
              </tbody>
            </table>
          </section> */}
          <Footer />
        </div>
      </>
    );
  }
}
export default Home;
