import React, { Component } from 'react';

// import { Col, Row } from 'react-bootstrap';
import { Helmet } from 'react-helmet';
import Header from '../components/Header';
import '../assets/styles/components/MapAndDirection.scss';
import Footer from '../components/Footer';
import secondSectionMapDirection from '../assets/static/images/background/second-section-map-direction.jpg';
import FavoriteSpots from '../components/FavoriteSpots';
import ScrollToTop from '../components/scroll-to-top';

// const position = [34.04792376863582, -118.45633978940867];

export default class MapAndDirection extends Component {
  // constructor(props) {
  //   super(props);
  //   this.state = {
  //     showMap: false,
  //     components: {},
  //   };
  //   this.loadClientComponents = this.loadClientComponents.bind(this);
  // }

  // loadClientComponents() {
  //   const { Map, Marker, Popup, TileLayer } = require('react-leaflet');
  //   this.setState({ showMap: true, components: { Map, Marker, Popup, TileLayer } });
  // }

  // componentDidMount() {
  //   this.loadClientComponents();
  // }

  render() {
    // const { Map, Marker, Popup, TileLayer } = this.state.components;
    return (
      <>
        <Helmet>
          <title>Brentwood Neighborhood, favorite Spots We Recommend</title>
          <meta
            name="description"
            content="Restaurants walking distance on San Vicente Boulevard, there are some of the best coffee shops around. Apartments are adjacent to Sawtelle Street and Japantown."
          />
        </Helmet>
        <ScrollToTop />
        <Header />
        <div id="content" className="bypass-block-target " tabIndex="-1">
          {/* <section className="description-amenities-map"> */}
          {/* <div className="width700 pr-5 pl-5"> */}
          {/* <p className="title-community-and-Apartment">Favorite spots we recommend</p> */}
          {/* <ul className="amenities-list "> */}
          {/* <p data-selenium-id="PCommAmenityli1">
                There are a myriad of fantastic restaurants just walking distance on San Vicente
                Boulevard, including: Baltaire, Pizzana, Katsuya and Kreation to name a few. In
                addition, there are some of the best coffee shops around: Alfred’s, Startbucks and
                Coral Tree. For all your shopping needs we are conveniently located minutes away
                from Whole Foods, Pavilion’s and Ralphs. The building is also adjacent to Sawtelle
                Street and Japantown with premiere sushi restaurants, ramen bars and shaved ice
                shops.
              </p>
              <p data-selenium-id="PCommAmenityli2">
                If you’re looking for a one stop shop for dining, shopping and events, there’s
                Brentwood Gardens Plaza and the Brentwood Country Mart.
              </p>
              <p data-selenium-id="PCommAmenityli3">
                For hiking, mountain biking and other outdoor activities, you’re just miles from
                some of the best trails in the Santa Monica mountains, Los Liones, Westridge and
                Temescal. Or you can take a quick drive to the beach for a surf session or watch the
                California sunset at the Santa Monica or Malibu Pier.
              </p>
              <p data-selenium-id="PCommAmenityli4">
                Brentwood Farmers Market is every Sunday from 9am-2pm with fresh produce and
                prepared food items weekly from local purveyors.
              </p> */}
          {/* <li data-selenium-id="PCommAmenityli5">
                  We’ve partnered with the Westwood Equinox to provide our residents with exclusive
                  memberships, and it’s a quick 5 minute drive away.
                </li> */}
          {/* </ul> */}
          {/* </div> */}
          {/* </section> */}
          <div className="container mapTop mapaAndDirection">
            {/* <p className="sr-only">images amenitie</p> */}
            {/* <h1 className="h2 text-center mb-5">Favorite Spots We Recommend</h1> */}
            <FavoriteSpots home={false} />
            <iframe title="Map Iframe" src="/map/" className="iframemap mt-5" />
          </div>
          {/* <div className="container second-section">
            <img src={secondSectionMapDirection} alt="section second map" />
          </div> */}

          {/* <section className="three-section-map d-flex flex-column flex-md-row justify-content-center flex-wrap align-items-stretch">
            <div className="w-50 mapcontact">
              {this.state.showMap && (
                <Map center={position} zoom={16} scrollWheelZoom={false}>
                  <TileLayer
                    url="https://{s}.basemaps.cartocdn.com/rastertiles/voyager/{z}/{x}/{y}{r}.png"
                    attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors &copy; <a href="https://carto.com/attributions">CARTO</a>'
                  />
                  <Marker position={position}>
                    <Popup>
                      <span>1330 Federal Ave</span>
                    </Popup>
                  </Marker>
                </Map>
              )}
            </div>
          </section> */}
        </div>
        <Footer />
      </>
    );
  }
}
