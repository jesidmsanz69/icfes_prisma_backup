import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Container, Breadcrumb } from 'react-bootstrap';
import { Helmet } from 'react-helmet';
import ScrollToTop from '../components/scroll-to-top';
import Header from '../components/Header';
import Footer from '../components/Footer';
// const classBreadcrumb = { className: 'justify-content-center' };

export default class Sitemap extends Component {
  // componentDidMount() {
  // this.props.validateActivePage();
  // window.validateActivePage();
  // }

  render() {
    return (
      <>
        <Helmet>
          <title>Abstract Site Map</title>
          <meta
            name="description"
            content="Browse through our website for more information about our electrical services in San Fernando Valley. Contact us today."
          />
        </Helmet>
        <ScrollToTop />

        <Header />
        <Container
          id="content"
          className="contentinner bypass-block-target content-section-p"
          tabIndex="-1"
        >
          <h1 className="h2 text-center">Sitemap</h1>
          <ul>
            <li className="mb-2">
              <Link to="/">Home</Link>
            </li>
            <li className="mb-2">
              <Link to="/floorplans">Availability</Link>
            </li>
            <li className="mb-2">
              <Link to="/photogallery">Gallery</Link>
            </li>
            <li className="mb-2">
              <Link to="/amenities">Amenities</Link>
            </li>
            <li className="mb-2">
              <Link to="/BrentwoodNeighborhood">Neighborhood</Link>
            </li>
            <li>
              <Link to="/contactUs">Contact Us</Link>
            </li>
          </ul>
        </Container>
        <Footer />
      </>
    );
  }
}
