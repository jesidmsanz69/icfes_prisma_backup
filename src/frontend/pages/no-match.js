import React from 'react';
import { Container, Breadcrumb } from 'react-bootstrap';
import { Helmet } from 'react-helmet';
import ScrollToTop from '../components/scroll-to-top';

const classBreadcrumb = { className: 'justify-content-center' };

class NoMatch extends React.Component {
  constructor(props) {
    super(props);
    const { staticContext } = props;
    if (staticContext) staticContext.status = 404;
  }

  componentDidMount() {
    // this.props.validateActivePage();
    window.validateActivePage();
  }

  render() {
    return (
      <Container id="content" className="contentinner bypass-block-target" tabIndex="-1">
        <Helmet>
          <title>404 Error - The page cannot be found!</title>
          <meta
            name="description"
            content="Please try again! If you receive this message a second time, please report this problem"
          />
        </Helmet>
        <ScrollToTop />
        <Breadcrumb className="navbreadcrumb" listProps={classBreadcrumb}>
          <Breadcrumb.Item href="/">HOME</Breadcrumb.Item>
          <Breadcrumb.Item active>404</Breadcrumb.Item>
        </Breadcrumb>
        <h1 className="h1">The page cannot be found!</h1>
        <p className="text-center">
          <strong>Please try again! &nbsp;&nbsp;</strong>
          <br />
          <br />
          If you receive this message a second time,
          <br />
          please report this problem at <a href="mailto:help@it49.com">help@it49.com</a>
        </p>
      </Container>
    );
  }
}
export default NoMatch;
