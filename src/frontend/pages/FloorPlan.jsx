import React, { Component } from 'react';

import { Container, Row, Col, Card, Form, Button } from 'react-bootstrap';
import { Helmet } from 'react-helmet';
import ScrollToTop from '../components/scroll-to-top';

import { Link } from 'react-router-dom';
import Header from '../components/Header';
import '../assets/styles/components/FloorPlan.scss';
import 'blueimp-gallery/css/blueimp-gallery.min.css';
import Footer from '../components/Footer';
import a1 from '../assets/static/images/floorPlan/101.jpg';
import a2 from '../assets/static/images/floorPlan/102-202-302-402-PH2.jpg';
import a3 from '../assets/static/images/floorPlan/103-203-303-403-PH3.jpg';
import a4 from '../assets/static/images/floorPlan/104-204-304-404-PH4.jpg';
import a5 from '../assets/static/images/floorPlan/106-206-306.jpg';
import a6 from '../assets/static/images/floorPlan/107.jpg';
import a7 from '../assets/static/images/floorPlan/201-301-401-PH1.jpg';
import a8 from '../assets/static/images/floorPlan/205-305.jpg';
import a9 from '../assets/static/images/floorPlan/207.jpg';
import a10 from '../assets/static/images/floorPlan/307-407.jpg';
import a11 from '../assets/static/images/floorPlan/308-408-PH7.jpg';
import a12 from '../assets/static/images/floorPlan/405.jpg';
import a13 from '../assets/static/images/floorPlan/406.jpg';
import a14 from '../assets/static/images/floorPlan/PH5.jpg';
import a15 from '../assets/static/images/floorPlan/PH6.jpg';
import CustomFormGroup from '../components/custom/CustomFormGroup';

const dd = [
  {
    id: 3,
    bed: 2,
    bath: 2,
    area: '1381',
    name: '103, PH #3',
    image: a3,
    price: '$3,950.00',
  },
  {
    id: 4,
    bed: 2,
    bath: 2,
    area: '1408',
    name: 'PH #4',
    image: a4,
    price: '$3,950.00',
  },
  {
    id: 5,
    bed: 2,
    bath: 2,
    area: '1372',
    name: '106, 306',
    image: a5,
    price: '$3,995.00',
  },
  // {
  //   id: 6,
  //   bed: 2,
  //   bath: 2,
  //   area: '1599',
  //   name: '107',
  //   image: a6,
  //   price: '$4,250.00',
  // },

  {
    id: 8,
    bed: 2,
    bath: 2,
    area: '1141',
    name: '305',
    image: a8,
    price: '$3,595.00',
  },

  {
    id: 10,
    bed: 2,
    bath: 2,
    area: '1257',
    name: '307',
    image: a10,
    price: '$3,950.00',
  },
  {
    id: 11,
    bed: 2,
    bath: 2,
    area: '1362',
    name: '308, PH #7',
    image: a11,
    price: '$3,895.00',
  },
  {
    id: 12,
    bed: 2,
    bath: 2,
    area: '1523',
    name: '405',
    image: a12,
    price: '$4,450.00',
  },
  {
    id: 13,
    bed: 2,
    bath: 2,
    area: '1247',
    name: '406',
    image: a13,
    price: '$4,895.00',
  },
  {
    id: 14,
    bed: 2,
    bath: 2,
    area: '1400',
    name: 'PH #5',
    image: a14,
    price: '$4,850.00',
  },
  {
    id: 15,
    bed: 2,
    bath: 2,
    area: '1264',
    name: 'PH #6',
    image: a15,
    price: '$5,250.00',
  },
];

const td = [
  {
    id: 1,
    bed: 3,
    bath: 2,
    area: '1771',
    name: '101',
    image: a1,
    price: '$4,250.00',
  },
  {
    id: 2,
    bed: 3,
    bath: 2,
    area: '1719',
    name: '102, 302, PH #2',
    image: a2,
    price: '$4,250.00',
  },
  {
    id: 7,
    bed: 3,
    bath: 2,
    area: '1742',
    name: '301, PH #1',
    image: a7,
    price: '$4,495.00',
  },
];

const tt = [
  {
    id: 9,
    bed: 3,
    bath: 3,
    area: '1907',
    name: '207',
    image: a9,
    price: '$4,695.00',
  },
];

export default class FloorPlan extends Component {
  constructor() {
    super();
    this.state = {
      form: { bed: 0, bath: 0 },
      data: dd.concat(td, tt),
      active: 1,
    };
    this.goBlueimp = this.goBlueimp.bind(this);
    this.handleChange = this.handleChange.bind(this);
    // this.handleFilterFloor = this.handleFilterFloor.bind(this);
    this.handleFilterUnitFloorPlan = this.handleFilterUnitFloorPlan.bind(this);
  }

  goBlueimp() {
    const GalleryPic = require('blueimp-gallery');
    document.getElementById('parentgallery').onclick = function(evt) {
      let event = evt;
      event = event || window.event;
      const target = event.target || event.srcElement,
        link = target.parentNode,
        options = { index: link, event },
        links = this.getElementsByClassName('imagefloorplan');
      if (options.index.className.toLowerCase() === 'imagefloorplan') {
        event.preventDefault();
        GalleryPic(links, options);
      }
    };
    const Gallery = require('blueimp-gallery');
    document.getElementById('linksgallery').onclick = function(evt) {
      let event = evt;
      event = event || window.event;
      const target = event.target || event.srcElement,
        link = target,
        options = { index: link, event },
        links = this.getElementsByClassName('btn-view-detail');
      if (options.index.className.toLowerCase() === 'btn-view-detail') {
        event.preventDefault();
        Gallery(links, options);
      }
    };
  }

  // handleFilterFloor() {
  //   const { bath, bed } = this.state.form;
  //   console.log('bath y bed', bath, bed);

  //   if (bath === 0 && bed === 0) {
  //     this.setState({ data: initialData });
  //   } else {
  //     if (bath === 0 || bed === 0) {
  //       if (bath !== 0) {
  //         const data = initialData.filter((item) => item.bath === bath);
  //         this.setState({ data });
  //         console.log('data :>> ', data);
  //       } else {
  //         const data = initialData.filter((item) => item.bed === bed);
  //         this.setState({ data });
  //         console.log('data :>> ', data);
  //       }
  //     } else {
  //       const data = initialData.filter((item) => item.bath === bath && item.bed === bed);
  //       this.setState({ data });
  //       console.log('data :>> ', data);
  //     }
  //   }
  // }

  handleFilterUnitFloorPlan(param) {
    console.log('param :>> ', param);
    if (param === 1) {
      const itemsVal = dd.concat(td, tt);
      this.setState({ data: itemsVal, active: param });
    } else if (param === 2) {
      this.setState({ data: td, active: param });
    } else if (param === 3) {
      this.setState({ data: dd, active: param });
    } else if (param === 4) {
      this.setState({ data: tt, active: param });
    }
  }

  handleChange(event) {
    // console.log('hola', event.target.name);
    // const { data } = this.state;
    // let dataResult;
    // if (event.target.name === 'bath') {
    //   dataResult = data.filter((item) => item.bath === parseInt(event.target.value, 10));
    // } else {
    //   dataResult = data.filter((item) => item.bed === parseInt(event.target.value, 10));
    // }
    // console.log('dataResult', dataResult);
    const { form } = this.state;
    // console.log('event', event);
    this.setState({
      form: {
        ...form,
        [event.target.name]: parseInt(event.target.value, 10),
      },
    });
  }

  componentDidMount() {
    this.goBlueimp();
  }

  render() {
    // console.log('this.state.data :>> ', this.state.data);
    return (
      <>
        <Helmet>
          <title>Luxury Apartments in Brentwood, Floor Plans and Availability</title>
          <meta
            name="description"
            content=" Luxury Apartments in Brentwood, Floor Plans and Availability,  3 Bed 3 Bath, 3 Bed 2 Bath, 2 Bed  2 Bath"
          />
        </Helmet>
        <ScrollToTop />
        <Header />
        <div id="content" className="bypass-block-target " tabIndex="-1">
          <Container className="content-floor-plan content-section-p">
            <h1 className="h2 text-center mb-5">Floor Plans and Availability</h1>
            <div className="text-center filter-floor">
              <a
                href="#!"
                className={`list-group-filter list-group-border-right ${this.state.active === 1 &&
                  'color-second'}`}
                onClick={() => this.handleFilterUnitFloorPlan(1)}
              >
                All
                <span className="sr-only">Show all content</span>
              </a>
              <a
                href="#!"
                className={`list-group-filter list-group-border-right ${this.state.active === 4 &&
                  'color-second'}`}
                onClick={() => this.handleFilterUnitFloorPlan(4)}
              >
                3 Bed - 3 Bath
              </a>
              <a
                href="#!"
                className={`list-group-filter list-group-border-right ${this.state.active === 2 &&
                  'color-second'}`}
                onClick={() => this.handleFilterUnitFloorPlan(2)}
              >
                3 Bed - 2 Bath
              </a>
              <a
                href="#!"
                className={`list-group-filter ${this.state.active === 3 && 'color-second'}`}
                onClick={() => this.handleFilterUnitFloorPlan(3)}
              >
                2 Bed - 2 Bath
              </a>
            </div>
            <div id="parentgallery">
              <div id="linksgallery" className="resultfloorplans">
                <Row className="mt-5">
                  {this.state.data.map((item) => (
                    <Col sm={4} key={item.id} className="pb-4">
                      <Card>
                        <Card.Body>
                          <Card.Title className="text-center">
                            {item.bed} Bed - {item.bath} Bath | Approx {item.area} sq.ft.
                          </Card.Title>
                          <Card.Subtitle className="subTitle mb-2 text-muted">
                            {item.name}
                          </Card.Subtitle>
                          <div className="text-center">
                            <div className="content-image">
                              <a href={item.image} className="imagefloorplan">
                                <img
                                  src={item.image}
                                  alt="floor plan 1"
                                  className="img-fluid mx-auto"
                                  // width="150px"
                                />
                              </a>
                            </div>
                            <p className="fp-price pt-5">
                              Starting at <span>{item.price}</span>/month
                            </p>
                            {/* <p className="fp-price m-0">Inquire for details</p> */}
                            <a href={item.image} className="btn-view-detail">
                              View
                            </a>
                            <Link to={`/contactUs/${item.bed}/${item.bath}`}>
                              Contact Us
                              <span className="sr-only">
                                property {item.bed}/{item.bath}
                              </span>
                            </Link>
                          </div>
                        </Card.Body>
                      </Card>
                    </Col>
                  ))}
                </Row>
              </div>
            </div>
          </Container>
        </div>
        <Footer />
        <div id="blueimp-gallery" className="blueimp-gallery blueimp-gallery-controls">
          <div className="slides" />
          <h3 className="title" />
          <a role="button" className="prev">
            ‹
          </a>
          <a role="button" className="next">
            ›
          </a>
          <a role="button" className="close">
            ×
          </a>
        </div>
      </>
    );
  }
}
