import { CHANGE_PASSWORD_REQUEST, USER_SUCCESS, USER_ERROR } from '../actions/userActions';

const INITIAL_STATE = {
  loading: false,
  hasError: false,
  error: null,
  data: null,
};

const registerReducers = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case CHANGE_PASSWORD_REQUEST:
      return {
        ...state,
        loading: true,
        data: null,
      };
    case USER_SUCCESS:
      return {
        ...state,
        loading: false,
        hasError: false,
        data: action.payload,
      };
    case USER_ERROR:
      return {
        ...state,
        loading: false,
        hasError: true,
        error: action.payload,
      };
    default:
      return state;
  }
};

export default registerReducers;
