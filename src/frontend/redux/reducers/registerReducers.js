import { REGISTER_REQUEST, REGISTER_SUCCESS, REGISTER_ERROR } from '../actions/registerActions';

const INITIAL_STATE = {
  loading: false,
  hasError: false,
  error: null,
  data: [],
};

const registerReducers = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case REGISTER_REQUEST:
      return {
        ...state,
        loading: true,
      };
    case REGISTER_SUCCESS:
      return {
        ...state,
        loading: false,
        hasError: false,
        data: action.payload,
      };
    case REGISTER_ERROR:
      return {
        ...state,
        loading: false,
        hasError: true,
        error: action.payload,
      };
    default:
      return state;
  }
};

export default registerReducers;
