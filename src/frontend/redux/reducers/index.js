import { combineReducers } from 'redux';
import registerReducers from './registerReducers';
import loginReducers from './loginReducers';
import userReducers from './userReducers';

const rootReducer = combineReducers({
  register: registerReducers,
  login: loginReducers,
  user: userReducers,
});

export default rootReducer;
