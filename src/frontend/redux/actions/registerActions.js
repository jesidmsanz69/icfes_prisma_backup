import axios from 'axios';

export const REGISTER_REQUEST = 'REGISTER_REQUEST';
export const REGISTER_SUCCESS = 'REGISTER_SUCCESS';
export const REGISTER_ERROR = 'REGISTER_ERROR';

const mainRoute = 'users';

const registerSuccess = (payload) => ({
  type: REGISTER_SUCCESS,
  payload,
});

const registerError = (payload) => ({
  type: REGISTER_ERROR,
  payload,
});
export const registerRequest = (data) => (dispatch) => {
  console.log('data', data.body);
  dispatch({ type: REGISTER_REQUEST });
  axios
    .post(mainRoute, data)
    .then(({ data }) => dispatch(registerSuccess(data.body)))
    .catch((error) => {
      dispatch(registerError(error));
      // return Promise.reject(error);
    });
};
