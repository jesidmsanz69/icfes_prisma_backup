import axios from 'axios';

export const CHANGE_PASSWORD_REQUEST = 'CHANGE_PASSWORD_REQUEST';
export const USER_SUCCESS = 'USER_SUCCESS';
export const USER_ERROR = 'USER_ERROR';

const mainRoute = 'users';

const userSuccess = (payload) => ({
  type: USER_SUCCESS,
  payload,
});

const userError = (payload) => ({
  type: USER_ERROR,
  payload,
});

export const changePasswordRequest = (data) => (dispatch) => {
  dispatch({ type: CHANGE_PASSWORD_REQUEST });
  axios
    .post(`${mainRoute}/change-password`, data)
    .then(({ data }) => dispatch(userSuccess(data.body)))
    .catch((error) => {
      dispatch(userError(error));
      // return Promise.reject(error);
    });
};

export const resetPasswordRequest = (data) => (dispatch) => {
  dispatch({ type: CHANGE_PASSWORD_REQUEST });
  axios
    .post(`${mainRoute}/reset-password`, data)
    .then(({ data }) => dispatch(userSuccess(data.body)))
    .catch((error) => {
      dispatch(userError(error));
      // return Promise.reject(error);
    });
};
