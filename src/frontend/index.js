import React from 'react';
import { hydrate, render } from 'react-dom';
import { Provider } from 'react-redux';
import { createStore, compose, applyMiddleware } from 'redux';
import { Router } from 'react-router';
import { createBrowserHistory } from 'history';
import thunk from 'redux-thunk';
import { loadableReady } from '@loadable/component';
// import cookie from 'js-cookie';
import axios from 'axios';

// import $ from 'jquery';
// import Popper from 'popper.js';
import { BrowserRouter } from 'react-router-dom';
import reducer from './redux/reducers';
import App from './routes/App';
import './assets/styles/Base.scss';
import { token } from './utils/auth';
import { config } from '../server/config';

// require('require-context/register');

// import 'bootstrap/dist/js/bootstrap.bundle.min';
// import 'react-bootstrap-typeahead/css/Typeahead.css';

if (typeof window !== 'undefined') {
  const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
  const preloadedState = window.__PRELOADED_STATE__;
  // const user = cookie.get('user') ? cookie.get('user') : null;
  // if (user) preloadedState.login.user = JSON.parse(user);
  // const store = createStore(reducer, preloadedState, composeEnhancers());
  const store = createStore(reducer, preloadedState, composeEnhancers(applyMiddleware(thunk)));
  const history = createBrowserHistory();

  delete window.__PRELOADED_STATE__;

  axios.defaults.baseURL = '/api/';
  axios.defaults.headers.common = { Authorization: `bearer ${token}` };

  //Redirect cuando el token se venza
  axios.interceptors.response.use(
    function(response) {
      return response;
    },
    function(error) {
      const originalRequest = error.config;
      if (error.response && error.response.status === 401 && !originalRequest._retry) {
        // if (window.location.href.indexOf('/login') === -1) history.push('/login');
        if (window.location.href.indexOf('/login') === -1) {
          window.location.href = `/login?returnUrl=${window.location.pathname}`;
        }
        // history.replace('/login');
      }
      return Promise.reject(error);
    }
  );
  if (!config.hydrate && config.dev) {
    loadableReady(() => {
      render(
        <Provider store={store}>
          <Router history={history}>
            <BrowserRouter>
              <App isLogged={preloadedState.login.user.id} />
            </BrowserRouter>
          </Router>
        </Provider>,
        document.getElementById('root')
      );
    });
  } else {
    loadableReady(() => {
      hydrate(
        <Provider store={store}>
          <Router history={history}>
            <BrowserRouter>
              <App isLogged={preloadedState.login.user.id} />
            </BrowserRouter>
          </Router>
        </Provider>,
        document.getElementById('root')
      );
    });
  }
}
